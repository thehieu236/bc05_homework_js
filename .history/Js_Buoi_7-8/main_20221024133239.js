// Tạo biến arr
var arr = [];
// var arr = [0, 1, 2, 1, 3, 4];

// Tạo hàm inKetQua để in kết quả ra cho từng bài.
var inKetQua = function (id, result) {
	return (document.getElementById(
		id
	).innerHTML = `<h5 class="text__result">${result}</h5>`);
};

// Tạo hàm lấy giá trị nhập vào thêm vào mảng.
var themSo = function () {
	var taoArr = function () {
		var nhapSoN = document.getElementById('nhapSoN').value * 1;

		// Đưa value lấy từ nhapSoN vào arr
		arr.push(nhapSoN);
		var val = '';

		for (index = 0; index < arr.length; index++) {
			if (index == 0) {
				val += arr[index];
			} else {
				val += ', ' + arr[index];
			}
		}
		return val;
	};

	inKetQua('result_array', taoArr());
};

// Bài 1: Tính tổng các số dương.
var tongSoDuong = function () {
	// Tạo biến sum
	var sum = 0;

	// Dùng vòng lặp tính tổng.
	for (i = 0; i < arr.length; i++) {
		sum += arr[i];
	}
	inKetQua('result_1', sum);
};

// Bài 2: Đếm có bao nhiêu số dương trong mảng.
var demSoDuong = function () {
	var count = 0;

	for (i = 0; i < arr.length; i++) {
		if (arr[i] > 0) {
			count++;
		}
	}

	inKetQua('result_2', count);
};

// Bài 3: Tìm số nhỏ nhất.
var soNhoNhat = function () {
	// Tạo biến minNumber
	var minNumber = arr[0];

	// Tạo vòng lặp kiểm tra số nhỏ nhất.
	for (i = 0; i < arr.length; i++) {
		if (minNumber > arr[i]) {
			//Nếu minNumber lớn hơn một số nào đó trong mảng thì ta cho minNumber sẽ bằng số đó, vậy minNumber sẽ luôn nhỏ nhất.
			minNumber = arr[i];
		}
	}

	inKetQua('result_3', minNumber);
};

// Bài 4: Tìm số dương nhỏ nhất.
var timSoNhoNhat = function () {
	// Tạo biến numMin.
	var numMin = 0;

	// Tạo vòng lặp kiểm tra lấy giá trị lớn hơn 0 đầu tiên trong mảng, nếu giá trị đầu bằng 0 thì vòng lặp sau sẽ không tìm được giá trị dương nhỏ nhất
	for (i = 0; i < arr.length; i++) {
		if (arr[i] > 0) {
			numMin = arr[i];
			break;
		}
	}

	// Tạo vòng lặp tìm giá trị dương nhỏ nhất.
	for (i = 0; i < arr.length; i++) {
		if (numMin > arr[i] && arr[i] > 0) {
			numMin = arr[i];
		}
	}

	// Nếu giá trị numMin = 0 thì tức là ko có số dương nào nhỏ nhất.
	if (numMin == 0) {
		numMin = 'Không có số dương nhỏ nhất nào.';
	}

	inKetQua('result_4', numMin);
};

// Bài 5: Tìm số chẵn cuối cùng.
var soChanCuoiCung = function () {
	// Tạo biến numEvenLast
	var numEvenLast = 1;

	// Tạo vòng lặp kiểm tra số chẵn từ cuối mảng trở lại
	for (i = arr.length - 1; i > 0; i--) {
		if (arr[i] % 2 == 0) {
			//Nếu số chia hết cho 2 tức số đó là số chẵn, ta return ngắt vòng lặp luôn.
			numEvenLast = arr[i];
			break;
		}
	}

	// Nếu không có số nào chia hết cho 2 thì kiểm tra lại và return nó về -1
	if (numEvenLast % 2 !== 0) {
		numEvenLast = -1;
	}

	inKetQua('result_5', numEvenLast);
};
console.log('soChanCuoiCung:', soChanCuoiCung());

//Bài 6: Đổi chỗ.
var doiSo = function () {
	var soThu1 = document.getElementById('nhapSo1').value * 1;
	var soThu2 = document.getElementById('nhapSo2').value * 1;

	var val1 = arr[soThu1];
	var val2 = arr[soThu2];
	for (i = 0; i < arr.length; i++) {
		if (soThu1 == i) {
			arr[i] = val2;
		}
		if (soThu2 == i) {
			arr[i] = val1;
		}
	}

	inKetQua('result_6', arr);
};

//show and hide dropdown click
function show_hide() {
	var click = document.querySelector('.dropdownn-menu');
	if (click.style.display === 'none') {
		click.style.display = 'block';
	} else {
		click.style.display = 'none';
	}
}

// Bài 7: Sắp xếp mảng theo thứ tự tăng dần.
var sapXepTangDan = function () {
	for(i = 0; i < arr.length; i++)
	var numMin = i

};
