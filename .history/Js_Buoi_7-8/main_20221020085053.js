// Tạo biến arr
var arr = [];

// Tạo hàm inKetQua để in kết quả ra cho từng bài.
var inKetQua = function (id, result) {
	return document.getElementById(
		id
	).innerHTML = `<h5 class="text__result">${result}</h5>`;
};

var themSo = function () {
	var taoArr = function () {
		var nhapSoN = document.getElementById('nhapSoN').value * 1;

		// Đưa value lấy từ nhapSoN vào arr
		arr.push(nhapSoN);
		var val = '';

		for (index = 0; index < arr.length; index++) {
			if (index == 0) {
				val += arr[index];
			} else {
				val += ', ' + arr[index];
			}
		}
		return val;
	};

	inKetQua('result_array', taoArr())

	// Bài 1: Tính tổng các số dương.
	var tongSoDuong = function () {
		// Tạo biến sum
		var sum = 0;

		// Dùng vòng lặp tính tổng.
		for (i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		return sum;
	};

	inKetQua('result_1', tongSoDuong())

	// Bài 2: Đếm có bao nhiêu số dương trong mảng.
	var demSoDuong = function () {
		var count = 0;

		for (i = 0; i < arr.length; i++) {
			if (arr[i] > 0) {
				count++;
			}
		}

		return count;
	};
	inKetQua('result_2', demSoDuong())

	// Bài 3: Tìm số dương.
	var soNhoNhat = function () {
		// Tạo biến minNumber
		var minNumber = arr[0];

		// Tạo vòng lặp kiểm tra số nhỏ nhất.
		for (i = 0; i < arr.length; i++) {
			if (minNumber > arr[i]) {
				//Nếu minNumber lớn hơn một số nào đó trong mảng thì ta cho minNumber sẽ bằng số đó, vậy minNumber sẽ luôn nhỏ nhất.
				minNumber = arr[i];
			}
		}

		return minNumber;
	};

	inKetQua('result_3', soNhoNhat())

	// Bài 4: Tìm số dương nhỏ nhất.
	var timSoNhoNhat = function () {
		// Tạo biến nmin là số dương nhỏ nhất với điều kiện nmin phải lớn hơn 0
		if (arr[0] > 0) {
			var nmin = arr[0];
		}

		// Tạo vòng lặp kiểm tra số dương nhỏ nhất.
		for (i = 0; i < arr.length; i++) {
			if (arr[i] >= 0 && arr[i] < nmin) {
				nmin = arr[i];
			}
		}

		// Nếu trong mảng nmin trả vế underfinded thì tức là không có số nào dương..
		if (nmin == undefined) {
			return 'Không có số dương nào trong mảng';
		}

		return nmin;
	};

	inKetQua('result_4', timSoNhoNhat())


	// Bài 5: Tìm số chẵn cuối cùng.
	var soChanCuoiCung = function () {
		// Tạo biến numEvenLast
		var numEvenLast = 1;

		// Tạo vòng lặp kiểm tra số chẵn từ cuối mảng trở lại
		for (i = arr.length - 1; i > 0; --i) {
			if (arr[i] % 2 == 0) {
				//Nếu số chia hết cho 2 tức số đó là số chẵn, ta return ngắt vòng lặp luôn.
				return (numEvenLast = arr[i]);
			}
		}

		// Nếu không có số nào chia hết cho 2 thì kiểm tra lại và return nó về -1
		if (numEvenLast % 2 !== 0) {
			return (numEvenLast = -1);
		}

		return numEvenLast;
	};

	inKetQua('result_5', soChanCuoiCung())

	//Bài 6: Đổi chỗ.
	var
};

//show and hide dropdown click
function show_hide() {
	var click = document.querySelector('.dropdownn-menu');
	if (click.style.display === 'none') {
		click.style.display = 'block';
	} else {
		click.style.display = 'none';
	}
}
