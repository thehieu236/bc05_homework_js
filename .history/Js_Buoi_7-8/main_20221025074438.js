// Tạo biến arr
var arr = [];
var arr = [4, 9, 6, 3, 2, 1, 3, 4];

// Tạo hàm inKetQua để in kết quả ra cho từng bài.
var inKetQua = function (id, result) {
	return (document.getElementById(
		id
	).innerHTML = `<h5 class="text__result">${result}</h5>`);
};

// Tạo hàm lấy giá trị nhập vào thêm vào mảng.
var themSo = function () {
	var taoArr = function () {
		var nhapSoN = document.getElementById('nhapSoN').value * 1;

		// Đưa value lấy từ nhapSoN vào arr
		arr.push(nhapSoN);
		var val = '';

		for (index = 0; index < arr.length; index++) {
			if (index == 0) {
				val += arr[index];
			} else {
				val += ', ' + arr[index];
			}
		}
		return val;
	};

	inKetQua('result_array', taoArr());
};

// Bài 1: Tính tổng các số dương.
var tongSoDuong = function () {
	// Tạo biến sum
	var sum = 0;

	// Dùng vòng lặp tính tổng.
	for (i = 0; i < arr.length; i++) {
		sum += arr[i];
	}
	inKetQua('result_1', sum);
};

// Bài 2: Đếm có bao nhiêu số dương trong mảng.
var demSoDuong = function () {
	var count = 0;

	for (i = 0; i < arr.length; i++) {
		if (arr[i] > 0) {
			count++;
		}
	}

	inKetQua('result_2', count);
};

// Bài 3: Tìm số nhỏ nhất.
var soNhoNhat = function () {
	// Tạo biến minNumber
	var minNumber = arr[0];

	// Tạo vòng lặp kiểm tra số nhỏ nhất.
	for (i = 0; i < arr.length; i++) {
		if (minNumber > arr[i]) {
			//Nếu minNumber lớn hơn một số nào đó trong mảng thì ta cho minNumber sẽ bằng số đó, vậy minNumber sẽ luôn nhỏ nhất.
			minNumber = arr[i];
		}
	}

	inKetQua('result_3', minNumber);
};

// Bài 4: Tìm số dương nhỏ nhất.
var timSoNhoNhat = function () {
	// Tạo biến numMin.
	var numMin = 0;

	// Tạo vòng lặp kiểm tra lấy giá trị lớn hơn 0 đầu tiên trong mảng, nếu giá trị đầu bằng 0 thì vòng lặp sau sẽ không tìm được giá trị dương nhỏ nhất
	for (i = 0; i < arr.length; i++) {
		if (arr[i] > 0) {
			numMin = arr[i];
			break;
		}
	}

	// Tạo vòng lặp tìm giá trị dương nhỏ nhất.
	for (i = 0; i < arr.length; i++) {
		if (numMin > arr[i] && arr[i] > 0) {
			numMin = arr[i];
		}
	}

	// Nếu giá trị numMin = 0 thì tức là ko có số dương nào nhỏ nhất.
	if (numMin == 0) {
		numMin = 'Không có số dương nhỏ nhất nào.';
	}

	inKetQua('result_4', numMin);
};

// Bài 5: Tìm số chẵn cuối cùng.
var soChanCuoiCung = function () {
	// Tạo biến numEvenLast
	var numEvenLast = 1;

	// Tạo vòng lặp kiểm tra số chẵn từ cuối mảng trở lại
	for (i = arr.length - 1; i > 0; i--) {
		if (arr[i] % 2 == 0) {
			//Nếu số chia hết cho 2 tức số đó là số chẵn, ta return ngắt vòng lặp luôn.
			numEvenLast = arr[i];
			break;
		}
	}

	// Nếu không có số nào chia hết cho 2 thì kiểm tra lại và return nó về -1
	if (numEvenLast % 2 !== 0) {
		numEvenLast = -1;
	}

	inKetQua('result_5', numEvenLast);
};

//Bài 6: Đổi chỗ.
var doiSo = function () {
	var soThu1 = document.getElementById('nhapSo1').value * 1;
	var soThu2 = document.getElementById('nhapSo2').value * 1;

	var val1 = arr[soThu1];
	var val2 = arr[soThu2];
	for (i = 0; i < arr.length; i++) {
		if (soThu1 == i) {
			arr[i] = val2;
		}
		if (soThu2 == i) {
			arr[i] = val1;
		}
	}

	inKetQua('result_6', arr);
};

//show and hide dropdown click
function show_hide() {
	var click = document.querySelector('.dropdownn-menu');
	if (click.style.display === 'none') {
		click.style.display = 'block';
	} else {
		click.style.display = 'none';
	}
}

// Bài 7: Sắp xếp mảng theo thứ tự tăng dần.
var sapXepTangDan = function () {
	// Tạo vòng lặp for
	for (i = 0; i < arr.length; i++) {
		// Tạo biến val là biến trung gian.
		var val = 0;
		for (j = 0; j < arr.length; j++) {
			// Ta so sánh từng số với phần còn lại của mảng, nếu số đó lớn hơn 1 số bất kỳ thì ta đổi chỗ hai số cho nhau thông qua val.
			if (arr[j] > arr[i]) {
				val = arr[j];
				arr[j] = arr[i];
				arr[i] = val;
			}
		}
	}

	inKetQua('result_7', arr);
};

// Bài 8: Tìm số nguyên tố đầu tiên trong mảng. Nếu mảng không có số nguyên tố thì trả về – 1.

var soNguyenToDauTien = function () {
	var soNguyenToDauTien = 0;
	// Tạo vòng lặp kiểm tra số nguyên tố.
	// for (i = 0; i < arr.length; i++) {
	// 	var flag = 0;
	// 	// Tạo biến số nguyên tố đầu tiên.
	// 	var n = arr[i];
	// 	// if ((n = 2)) {
	// 	// 	soNguyenToDauTien = n;
	// 	// 	break;
	// 	// }

	// 	if (n > 2) {
	// 		console.log(n);
	// 		for (j = 2; j < Math.sqrt(n); j++) {
	// 			if (n % j === 0) {
	// 				flag = 1;
	// 			}
	// 		}
	// 		console.log(j);
	// 		console.log(flag);
	// 	}
	// }

	// Lớn hơn 2

	// Không chia hết cho các số từ 2 đến căn bậc hai của chính nó.

	// Số nguyên tố đầu tiên trong mảng.

	// Tạo vòng lặp kiểm tra từng số trong mảng.
	for (i = 0; i < arr.length; i++) {
		var index = arr[i];
		if (index >= 2) {
			// Tạo vòng lặp for kiểm tra, nếu số đó không chia hết cho bất kì số nào từ 2 cho tới giá trị căn bậc hai của nó thì nó chính là số nguyên.

			var kiemTraSoNguyenTo = function (i) {
				for (j = 2; j <= Math.sqrt(index); j++) {
					var val = j;
					if (index % val !== 0) {
						soNguyenToDauTien = index;
					}
				}
					return;
			};

			kiemTraSoNguyenTo(index);
			console.log(
				'file: main.js ~ line 214 ~ soNguyenToDauTien ~ soNguyenToDauTien',
				soNguyenToDauTien
			);
		}
	}

	console.log(
		'file: main.js ~ line 177 ~ soNguyenToDauTien ~ soNguyenToDauTien',
		soNguyenToDauTien
	);

	inKetQua('result_8', soNguyenToDauTien);
};
console.log(soNguyenToDauTien());

// console.log('inKetQua():', themSo());
