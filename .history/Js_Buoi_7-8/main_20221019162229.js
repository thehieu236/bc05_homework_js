// Tạo biến arr
var arr = [];
var themSo = function () {
	var nhapSoN = document.getElementById('nhapSoN').value * 1;

	console.log('arr:', arr);

	// Đưa value lấy từ nhapSoN vào arr
	arr.push(nhapSoN);

	var val = '';

	for (index = 0; index < arr.length; index++) {
		if (index == 0) {
			val += arr[index];
		} else {
			val += ', ' + arr[index];
		}
	}

	document.getElementById(
		'result_array'
	).innerHTML = `<h5 class="text__result">${val}</h5>`;

	// Bài 1: Tính tổng các số dương.
	// Tạo biến sum
	var sum = 0;

	// Dùng vòng lặp tính tổng.
	for (i = 0; i < arr.length; i++) {
		sum += arr[i];
	}
	console.log(sum);

	document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result">${sum}</h5>`;
};
