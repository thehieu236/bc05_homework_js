// Tạo biến arr
var arr = [];

// Tạo hàm inKetQua để in kết quả ra cho từng bài.
var inKetQua = function (id, result) {
	return (document.getElementById(
		id
	).innerHTML = `<h5 class="text__result">${result}</h5>`);
};

// Tạo hàm lấy giá trị nhập vào thêm vào mảng.
var themSo = function () {
	var taoArr = function () {
		var nhapSoN = document.getElementById('nhapSoN').value * 1;

		// Đưa value lấy từ nhapSoN vào arr
		arr.push(nhapSoN);
		var val = ' ';

		for (index = 0; index < arr.length; index++) {
			switch (index) {
				case 0:
					val += arr[index];
					break;
				default:
					val += ', ' + arr[index];
					break;
			}
		}
		return val;
	};

	inKetQua('result_array', taoArr());
};

// Tạo function drop down menu
//show and hide dropdown click function
function dropDownMenu(cl) {
	var click = document.querySelector(cl);
	if (click.style.display === 'none') {
		click.style.display = 'block';
	} else {
		click.style.display = 'none';
	}
}

// Bài 1: Tính tổng các số dương.
var tongSoDuong = function () {
	// Tạo biến sum
	var sum = 0;

	// Dùng vòng lặp tính tổng.
	for (i = 0; i < arr.length; i++) {
		// Để tính tổng ta có điều kiện giá trị trong mảng phải dương.
		var num = arr[i];
		if (num > 0) {
			sum += num;
		}
	}
	inKetQua('result_1', sum);
};

// Bài 2: Đếm có bao nhiêu số dương trong mảng.
var demSoDuong = function () {
	var count = 0;

	for (i = 0; i < arr.length; i++) {
		if (arr[i] > 0) {
			count++;
		}
	}

	inKetQua('result_2', count);
};

// Bài 3: Tìm số nhỏ nhất.
var soNhoNhat = function () {
	// Tạo biến minNumber
	var minNumber = arr[0];

	// Tạo vòng lặp kiểm tra số nhỏ nhất.
	for (i = 0; i < arr.length; i++) {
		if (minNumber > arr[i]) {
			//Nếu minNumber lớn hơn một số nào đó trong mảng thì ta cho minNumber sẽ bằng số đó, vậy minNumber sẽ luôn nhỏ nhất.
			minNumber = arr[i];
		}
	}

	inKetQua('result_3', minNumber);
};

// Bài 4: Tìm số dương nhỏ nhất.
var timSoNhoNhat = function () {
	// Tạo biến numMin.
	var numMin = 0;

	// Tạo vòng lặp kiểm tra lấy giá trị lớn hơn 0 đầu tiên trong mảng, nếu giá trị đầu bằng 0 thì vòng lặp sau sẽ không tìm được giá trị dương nhỏ nhất
	for (i = 0; i < arr.length; i++) {
		if (arr[i] > 0) {
			numMin = arr[i];
			break;
		}
	}

	// Tạo vòng lặp tìm giá trị dương nhỏ nhất.
	for (i = 0; i < arr.length; i++) {
		if (numMin > arr[i] && arr[i] > 0) {
			numMin = arr[i];
		}
	}

	// Nếu giá trị numMin = 0 thì tức là ko có số dương nào nhỏ nhất.
	if (numMin == 0) {
		numMin = 'Không có số dương nhỏ nhất nào.';
	}

	inKetQua('result_4', numMin);
};

// Bài 5: Tìm số chẵn cuối cùng.
var soChanCuoiCung = function () {
	// Tạo biến numEvenLast
	var numEvenLast = function () {
		// Tạo vòng lặp kiểm tra số chẵn từ cuối mảng trở lại
		for (i = arr.length - 1; i > 0; i--) {
			var num = arr[i];
			if (num % 2 == 0) {
				//Nếu số chia hết cho 2 tức số đó là số chẵn, ta return kết quả và ngắt vòng lặp luôn.
				return num;
			}
		}

		// Nếu không có số nào chia hết cho 2 thì trả kết quả bằng -1
		return -1;
	};

	inKetQua('result_5', numEvenLast());
};

//Bài 6: Đổi chỗ.
var doiSo = function () {
	var soThu1 = document.getElementById('nhapSo1').value * 1;
	var soThu2 = document.getElementById('nhapSo2').value * 1;

	var val1 = arr[soThu1];
	var val2 = arr[soThu2];
	for (i = 0; i < arr.length; i++) {
		if (soThu1 == i) {
			arr[i] = val2;
		}
		if (soThu2 == i) {
			arr[i] = val1;
		}
	}

	inKetQua('result_6', arr);
};

//show and hide dropdown click
function show_hide() {
	dropDownMenu('.dropdownn-menu');
}

// Bài 7: Sắp xếp mảng theo thứ tự tăng dần.
var sapXepTangDan = function () {
	// Tạo vòng lặp for
	for (i = 0; i < arr.length; i++) {
		// Tạo biến val là biến trung gian.
		var val = 0;
		for (j = 0; j < arr.length; j++) {
			// Ta so sánh từng số với phần còn lại của mảng, nếu số đó lớn hơn 1 số bất kỳ thì ta đổi chỗ hai số cho nhau thông qua val.
			if (arr[j] > arr[i]) {
				val = arr[j];
				arr[j] = arr[i];
				arr[i] = val;
			}
		}
	}

	inKetQua('result_7', arr);
};

// Bài 8: Tìm số nguyên tố đầu tiên trong mảng. Nếu mảng không có số nguyên tố thì trả về – 1.

var soNguyenToDauTien = function () {
	function laySoNguyenToDauTien() {
		// Tạo hàm kiểm tra số nguyên tố.
		var ktSoNguyenTo = function (n) {
			var flag = 1;
			// Nếu flag = 0 thì số đó không phải là số nguyên tố
			// Nếu flag = 1 thì số đó là số nguyên tố
			if (n < 2) {
				return (flag = 0);
			} else if (n >= 2) {
				// Ta tạo vòng lặp kiểm tra các giá trị nếu chia hết cho các số từ 2 tới căn bậc hai của chính nó thì không phải là số nguyên tố
				for (i = 2; i <= Math.sqrt(n); i++) {
					if (n % i === 0) {
						flag = 0;
					}
				}
			}

			// Còn lại là số nguyên tố

			return flag;
		};

		// Tạo vòng lặp kiểm tra từng số trong mảng, nếu hàm trả vế 1 tứ là số nguyên tố thì ta kết thúc vòng lặp luôn.
		for (j = 0; j < arr.length; j++) {
			switch (ktSoNguyenTo(arr[j])) {
				case 1:
					return arr[j];
			}
		}

		// Nếu không có giá trị nào thoả mãn thì ta trả kết quả là -1.
		return -1;
	}

	inKetQua('result_8', laySoNguyenToDauTien());
};

// Bài 9: Nhập thêm 1 mảng số thực, tìm xem trong mảng có bao nhiêu số nguyên?

// Đầu tiên ta tạo một mảng mới cho bài 9.
var arrB9 = [];
// Tạo hàm lấy giá trị từ ngoài vào thêm vào mảng mới tạo
var themSoB9 = function () {
	// Taọ hàm đẩy các giá trị lấy vào lên mảng mới tạo và in kết quả.
	var taoArrB9 = function () {
		var valB9 = document.getElementById('nhapSoB9').value * 1;
		arrB9.push(valB9);
		var val = '';
		for (i = 0; i < arrB9.length; i++) {
			switch (i) {
				case 0:
					val += arrB9[i];
					break;
				default:
					val += ', ' + arrB9[i];
					break;
			}
		}
		return val;
	};

	inKetQua('result_arr_b9', taoArrB9());
};

// Tạo hàm kiểm tra và đếm có bao nhiêu số nguyên ở trong mảng. Ta sử dụng Number.isInteger để kiểm tra số nguyên.
var demSoNguyen = function () {
	var kiemTraSoNguyen = function () {
		var count = 0;

		// Tạo vòng lặp kiểm tra và đếm số nguyên.
		for (i = 0; i < arrB9.length; i++) {
			const temp = arrB9[i];
			if (Number.isInteger(temp) === true) {
				count++;
			}
		}
		return count;
	};

	inKetQua('result_9', kiemTraSoNguyen());
};

//show and hide dropdown click
function show_hide_b9() {
	dropDownMenu('.dropdownn-menu-b9');
}

//  Bài 10: So sánh số lượng số dương và số lượng số âm xem số nào nhiều hơn
var soSanhSoAvaD = function () {
	// Tạo hàm kiểm tra, đếm và so sánh số dương và âm
	function ktSoAmVaDuong() {
		// Tạo 2 biến số đếm âm và dương, sử dụng vòng lặp để kiểm tra các giá trị trong mảng nếu là số âm thì tăng biến countAm lên 1, dương thì tăng countDuong lên 1
		var countAm = 0;
		var countDuong = 0;
		for (i = 0; i < arr.length; i++) {
			const element = arr[i];
			if (element < 0) {
				countAm++;
			}
			if (element > 0) {
				countDuong++;
			}
		}

		// Ta tiến hành so sánh các số đếm được âm và dương, sau đó trả về kết quả.
		if (countAm > countDuong) {
			return 'Số âm > Số dương';
		}
		if (countAm < countDuong) {
			return ' Số dương > Số âm';
		} else {
			return 'Số âm = Số dương';
		}
	}

	inKetQua('result_10', ktSoAmVaDuong());
};

//show and hide dropdown click
function show_hide_b10() {
	dropDownMenu('.dropdownn-menu-b10');
}
