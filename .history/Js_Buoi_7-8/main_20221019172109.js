// Tạo biến arr
var arr = [];
var themSo = function () {
	var taoArr = function () {
		var nhapSoN = document.getElementById('nhapSoN').value * 1;

		console.log('arr:', arr);

		// Đưa value lấy từ nhapSoN vào arr
		arr.push(nhapSoN);

		var val = '';

		for (index = 0; index < arr.length; index++) {
			if (index == 0) {
				val += arr[index];
			} else {
				val += ', ' + arr[index];
			}
		}
		return val;
	};

	document.getElementById(
		'result_array'
	).innerHTML = `<h5 class="text__result">${taoArr()}</h5>`;

	// Bài 1: Tính tổng các số dương.
	var tongSoDuong = function () {
		// Tạo biến sum
		var sum = 0;

		// Dùng vòng lặp tính tổng.
		for (i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		return sum;
	};

	document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result">${tongSoDuong()}</h5>`;

	// Bài 2: Đếm có bao nhiêu số dương trong mảng.
	var demSoDuong = function () {
		var count = 0;

		for (i = 0; i < arr.length; i++) {
			if (arr[i] > 0) {
				count++;
			}
		}

		return count;
	};
	document.getElementById(
		'result_2'
	).innerHTML = `<h5 class="text__result">${demSoDuong()}</h5>`;

    // Bài 3: Tìm số dương.

    // Bài 4: Tìm số dương nhỏ nhất.
	var timSoNhoNhat = function () {
		var nmin = arr[0];
		for (i = 1; i < arr.length; i++) {
			if (nmin < arr[i]) {
				nmin == arr[i];
			}
		}
		return nmin;
	};

	document.getElementById(
		'result_3'
	).innerHTML = `<h5 class="text__result">${timSoNhoNhat()}</h5>`;
};

// console.log(themSo());
