// Tạo biến arr
var arr = [];
var themSo = function () {
	var taoArr = function () {
		var nhapSoN = document.getElementById('nhapSoN').value * 1;

		console.log('arr:', arr);

		// Đưa value lấy từ nhapSoN vào arr
		arr.push(nhapSoN);

		var val = '';

		for (index = 0; index < arr.length; index++) {
			if (index == 0) {
				val += arr[index];
			} else {
				val += ', ' + arr[index];
			}
		}
		return val;
	};

	document.getElementById(
		'result_array'
	).innerHTML = `<h5 class="text__result">${taoArr()}</h5>`;

	// Bài 1: Tính tổng các số dương.
	var tongSoDuong = function () {
		// Tạo biến sum
		var sum = 0;

		// Dùng vòng lặp tính tổng.
		for (i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		return sum;
	};

	document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result">${tongSoDuong()}</h5>`;

	// Bài 2: Đếm có bao nhiêu số dương trong mảng.
	var demSoDuong = function () {
		var count = 0;

		for (i = 0; i < arr.length; i++) {
			if (arr[i] > 0) {
				count++;
			}
		}

		return count;
	};
	document.getElementById(
		'result_2'
	).innerHTML = `<h5 class="text__result">${demSoDuong()}</h5>`;

	// Bài 3: Tìm số dương.
    var soNhoNhat = function(){
        // Tạo biến min
    }

	// Bài 4: Tìm số dương nhỏ nhất.
	var timSoNhoNhat = function () {
		// Tạo biến nmin là số dương nhỏ nhất với điều kiện nmin phải lớn hơn 0
		if (arr[0] > 0) {
			var nmin = arr[0];
		}

		// Tạo vòng lặp kiểm tra số dương nhỏ nhất.
		for (i = 0; i < arr.length; i++) {
			if (arr[i] >= 0 && arr[i] < nmin) {
				nmin = arr[i];
			}
		}

		// Nếu trong mảng nmin trả vế underfinded thì tức là không có số nào dương..
		if (nmin == undefined) {
			return 'Không có số dương nào trong mảng';
		}

		console.log('nmin:', nmin);
		return nmin;
	};

	console.log(timSoNhoNhat());

	document.getElementById(
		'result_4'
	).innerHTML = `<h5 class="text__result">${timSoNhoNhat()}</h5>`;
};

// console.log(themSo());
