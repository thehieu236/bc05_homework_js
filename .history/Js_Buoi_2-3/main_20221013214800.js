
/**
 * Bài 1: Tính tiền lương nhân viên.
 * Input: Nhập số ngày làm.
 *
 * Step:
 *  +Step 1: Tạo biến workDay.
 *  +Step 2: Tạo biến salary.
 *  +Step 3: Nhân workDay với salary.
 *
 *
 * Output: Tổng số tiền lương nhận được.
 */
function result_1() {
	var oneDaySalary = document.getElementById('one_day_salary').value * 1;
	var numberOfWorkingDay =
		document.getElementById('number_of_working_day').value * 1;

	var result = oneDaySalary * numberOfWorkingDay;
	var resultFormat = new Intl.NumberFormat('en-US').format(result);
	document.getElementById(
		'result'
	).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${resultFormat} (USD)</h5>`;
}

/**
 * Bài 2: Tính giá trị trung bình.
 *
 * Input: 1 2 3 4 5
 *
 * Step:
 *  + Step 1: Tạo các biến.
 *  + Step 2: Cộng các biến lại rồi chia cho 5.
 *
 * Output: 3
 */

function result_2() {
	var a = document.getElementById('a').value * 1;
	var b = document.getElementById('b').value * 1;
	var c = document.getElementById('c').value * 1;
	var d = document.getElementById('d').value * 1;
	var e = document.getElementById('e').value * 1;

	var result_2 = (a + b + c + d + e) / 5;

	document.getElementById(
		'result_2'
	).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${result_2}</h5>`;
}

// Bài 3:
function result_3() {
	var exchange = document.getElementById('exchange').value * 1;

	var result_3 = exchange * 23500;
	var result_3Format = new Intl.NumberFormat('vn-VN').format(result_3);

	document.getElementById(
		'result_3'
	).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${result_3Format} (VND)</h5>`;
}

// Bài 4:
function result_4() {
	var height = document.getElementById('height').value * 1;
	var width = document.getElementById('width').value * 1;

	var result_4_s = height * width;
	var result_4_c = (height + width) * 2;

	document.getElementById(
		'result_4'
	).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">Diện tích: ${result_4_s}</h5><h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">Chu vi: ${result_4_c}</h5>`;
}

// Bài 5:
function result_5() {
	var sum = document.getElementById('sum').value * 1;

	var unit = sum % 10;
	var ten = Math.floor(sum / 10);

	var result_5 = ten + unit;
	console.log('result_5: ', result_5);
	document.getElementById(
		'result_5'
	).innerHTML = `</h5><h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${result_5}</h5>`;
}
