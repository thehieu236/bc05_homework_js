// Bài 1: Tính ngày tháng năm

// Hàm kiểm tra xem ngày trước đó là ngày nào.
var ngayTruocDo = function () {
	var d = document.getElementById('date_1').value * 1;
	var m = document.getElementById('date_2').value * 1;
	var y = document.getElementById('date_3').value * 1;

	// Hàm kiểm tra năm nhuận.
	var namNhuan = function (y) {
		return ((y % 4 == 0 && y % 100 !== 0) || y % 400 == 0);
	};

	// Hàm kiểm tra số ngày tối đa trong tháng.
	var soNgayTrongThang = function (m) {
		switch (m) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return 31;
			case 2: {
				if (namNhuan(y) == true) {
					return 29;
				}
				return 28;
			}
			case 4:
			case 6:
			case 9:
			case 11:
				return 30;
		}

		return alert('Bạn đã nhập sai tháng, vui lòng nhập lại.');
	};

	// Hàm kiểm tra xem giá trị nhập vào có đúng ngày, tháng không ?
	var checkDM = function () {
		return (y > 0 && m > 0 && m < 13 && d > 0 && d <= soNgayTrongThang(m));
	};

	// Tạo 3 biến có 3 tham số là: d, m, y
	var td = d;
	var tm = m;
	var ty = y;

	// Nếu ngày, tháng thỏa mãn điều kiện hàm checkDM thì trừ đi 1
	if (checkDM() == true) {
		td = d - 1;
		// Kiểm tra ngày đầu tháng.
		// Nếu tháng không phải 1 (tức là đầu năm), không phải tháng 3 và số ngày bằng 1 (tức là đầu tháng) thì tháng trừ đi 1 và ngày bằng số ngày tối đa của tháng trước đó.
		if (m !== 1 && d == 1) {
			tm = m - 1;
			td = soNgayTrongThang(tm);
		}

		// Kiểm tra ngày đầu của năm.
		// Nếu tháng bằng 1 và số ngày bằng 1 thì tháng trước đó bằng 12(tm = 12), ngày bằng 30 và năm giảm đi 1(ty - 1).
		else if (m == 1 && d == 1) {
			td = 30;
			tm = 12;
			ty = y - 1;
		}
	}

	// Còn lại chắc chắn nhập sai ngày, yêu cầu nhập lại.
	else {
		return alert('Bạn đã nhập sai ngày, vui lòng nhập lại.');
	}

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${td}/${tm}/${ty}</h5>`);
};
// Hàm kiểm tra xem ngày tiếp theo là ngày nào.
var ngayTiepTheo = function () {
	var d = document.getElementById('date_1').value * 1;
	var m = document.getElementById('date_2').value * 1;
	var y = document.getElementById('date_3').value * 1;

	// Hàm kiểm tra năm nhuận.
	var namNhuan = function (y) {
		return (y % 4 == 0 && y % 100 !== 0) || y % 400 == 0;
	};

	// Hàm kiểm tra số ngày tối đa trong tháng.
	var soNgayTrongThang = function (m) {
		switch (m) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return 31;
			case 2: {
				if (namNhuan(y) == true) {
					return 29;
				}
				return 28;
			}
			case 4:
			case 6:
			case 9:
			case 11:
				return 30;
		}
		return alert('Bạn đã nhập sai tháng, vui lòng nhập lại.');
	};

	// Hàm kiểm tra xem giá trị nhập vào có đúng ngày, tháng không ?
	var checkDM = function () {
		return ( y > 0 && m > 0 && m < 13 && d > 0 && d <= soNgayTrongThang(m));
	};

	// Tạo 3 biến có 3 tham số là: d, m, y
	var nd = d;
	var nm = m;
	var ny = y;

	// Nếu ngày, tháng thỏa mãn điều kiện hàm checkDM thì cộng thêm 1
	if (checkDM() == true) {
		nd = d + 1;
		// Kiểm tra ngày cuối tháng.
		// Nếu tháng không phải 12 (tức là cuối năm) và số ngày bằng với số ngày trong tháng (tức là cuối tháng) thì tháng cộng thêm 1 và ngày bằng 1..
		if (m !== 12 && d == soNgayTrongThang(m)) {
			nd = 1;
			nm = m + 1;
		}

		// Kiểm tra ngày cuôí của năm.
		// Nếu tháng bằng 12 và số ngày bằng số ngày tối đa trong tháng(bằng 31) thì tháng bằng 1(nm = 1), ngày bằng 1(nd = 1) và năm tăng lên 1(ny + 1).
		else if ((m = 12 && d == soNgayTrongThang(m))) {
			nd = 1;
			nm = 1;
			ny = y + 1;
		}

		// Kiểm tra năm nhuận.
		// Nếu tháng bằng 2 và ngày bằng 28 hoặc 29 thì ta tăng tháng lên 1 và ngày = 1.
		else if (m == 2) {
			switch (namNhuan(y)) {
				case 29:
				case 28:
					nd = 1;
					nm = m + 1;
					break;
			}
		}
	}

	// Còn lại chắc chắn nhập sai ngày, yêu cầu nhập lại.
	else {
		return alert('Bạn đã nhập sai ngày, vui lòng nhập lại.');
	}

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${nd}/${nm}/${ny}</h5>`);
};

// Bài 2: Tính ngày.
// Hàm tính ngày.
var tinhNgay = function(){

	// lấy giá trị nhập vào.
	var m = document.getElementById('month').value * 1;
	var y = document.getElementById('year').value * 1;

	// Xét điêu
}

