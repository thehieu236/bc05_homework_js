// Bài 1: Tính ngày tháng năm

// Hàm kiểm tra xem ngày trước đó là ngày nào.
var ngayTruocDo = function () {
	var d = document.getElementById('date_1').value * 1;
	var m = document.getElementById('date_2').value * 1;
	var y = document.getElementById('date_3').value * 1;

	// Hàm kiểm tra năm nhuận.
	var namNhuan = function (y) {
		return (y % 4 == 0 && y % 100 !== 0) || y % 400 == 0;
	};

	// Hàm kiểm tra số ngày tối đa trong tháng.
	var soNgayTrongThang = function (m) {
		switch (m) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return 31;
			case 2: {
				if (namNhuan(y) == true) {
					return 29;
				}
				return 28;
			}
			case 4:
			case 6:
			case 9:
			case 11:
				return 30;
		}

		return alert('Bạn đã nhập sai tháng, vui lòng nhập lại.');
	};

	// Hàm kiểm tra xem giá trị nhập vào có đúng ngày, tháng không ?
	var checkDM = function () {
		return y > 0 && m > 0 && m < 13 && d > 0 && d <= soNgayTrongThang(m);
	};

	// Tạo 3 biến có 3 tham số là: d, m, y
	var td = d;
	var tm = m;
	var ty = y;

	// Nếu ngày, tháng thỏa mãn điều kiện hàm checkDM thì trừ đi 1
	if (checkDM() == true) {
		td = d - 1;
		// Kiểm tra ngày đầu tháng.
		// Nếu tháng không phải 1 (tức là đầu năm), không phải tháng 3 và số ngày bằng 1 (tức là đầu tháng) thì tháng trừ đi 1 và ngày bằng số ngày tối đa của tháng trước đó.
		if (m !== 1 && d == 1) {
			tm = m - 1;
			td = soNgayTrongThang(tm);
		}

		// Kiểm tra ngày đầu của năm.
		// Nếu tháng bằng 1 và số ngày bằng 1 thì tháng trước đó bằng 12(tm = 12), ngày bằng 30 và năm giảm đi 1(ty - 1).
		else if (m == 1 && d == 1) {
			td = 30;
			tm = 12;
			ty = y - 1;
		}
	}

	// Còn lại chắc chắn nhập sai ngày, yêu cầu nhập lại.
	else {
		return alert('Bạn đã nhập sai ngày, vui lòng nhập lại.');
	}

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${td}/${tm}/${ty}</h5>`);
};
// Hàm kiểm tra xem ngày tiếp theo là ngày nào.
var ngayTiepTheo = function () {
	var d = document.getElementById('date_1').value * 1;
	var m = document.getElementById('date_2').value * 1;
	var y = document.getElementById('date_3').value * 1;

	// Hàm kiểm tra năm nhuận.
	var namNhuan = function (y) {
		return (y % 4 == 0 && y % 100 !== 0) || y % 400 == 0;
	};

	// Hàm kiểm tra số ngày tối đa trong tháng.
	var soNgayTrongThang = function (m) {
		switch (m) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return 31;
			case 2: {
				if (namNhuan(y) == true) {
					return 29;
				}
				return 28;
			}
			case 4:
			case 6:
			case 9:
			case 11:
				return 30;
		}
		return alert('Bạn đã nhập sai tháng, vui lòng nhập lại.');
	};

	// Hàm kiểm tra xem giá trị nhập vào có đúng ngày, tháng không ?
	var checkDM = function () {
		return y > 0 && m > 0 && m < 13 && d > 0 && d <= soNgayTrongThang(m);
	};

	// Tạo 3 biến có 3 tham số là: d, m, y
	var nd = d;
	var nm = m;
	var ny = y;

	// Nếu ngày, tháng thỏa mãn điều kiện hàm checkDM thì cộng thêm 1
	if (checkDM() == true) {
		nd = d + 1;
		// Kiểm tra ngày cuối tháng.
		// Nếu tháng không phải 12 (tức là cuối năm) và số ngày bằng với số ngày trong tháng (tức là cuối tháng) thì tháng cộng thêm 1 và ngày bằng 1..
		if (m !== 12 && d == soNgayTrongThang(m)) {
			nd = 1;
			nm = m + 1;
		}

		// Kiểm tra ngày cuôí của năm.
		// Nếu tháng bằng 12 và số ngày bằng số ngày tối đa trong tháng(bằng 31) thì tháng bằng 1(nm = 1), ngày bằng 1(nd = 1) và năm tăng lên 1(ny + 1).
		else if ((m = 12 && d == soNgayTrongThang(m))) {
			nd = 1;
			nm = 1;
			ny = y + 1;
		}

		// Kiểm tra năm nhuận.
		// Nếu tháng bằng 2 và ngày bằng 28 hoặc 29 thì ta tăng tháng lên 1 và ngày = 1.
		else if (m == 2) {
			switch (namNhuan(y)) {
				case 29:
				case 28:
					nd = 1;
					nm = m + 1;
					break;
			}
		}
	}

	// Còn lại chắc chắn nhập sai ngày, yêu cầu nhập lại.
	else {
		return alert('Bạn đã nhập sai ngày, vui lòng nhập lại.');
	}

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${nd}/${nm}/${ny}</h5>`);
};

// Bài 2: Tính ngày.
// Hàm tính ngày.
var tinhNgay = function () {
	// lấy giá trị nhập vào.
	var m = document.getElementById('month').value * 1;
	var y = document.getElementById('year').value * 1;

	// Tính năm nhuận.
	var namNhuan = function () {
		return (y % 4 == 0 && y % 100 !== 0) || y % 400 == 0;
	};

	// Kiểm tra dữ liệu nhập vào.
	if (y <= 0) {
		return alert('Nhập sai dữ liệu, vui lòng nhập lại.');
	}

	// Hàm xét điều kiện của mỗi tháng có tối đa bao nhiêu ngày ?
	var soNgayTrongThang = function () {
		switch (m) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return 31;
			case 2:
				if (namNhuan()) {
					return 29;
				} else {
					return 28;
				}
			case 4:
			case 6:
			case 9:
			case 10:
			case 11:
				return 30;
		}

		return alert('Nhập sai dữ liệu, vui lòng nhập lại.');
	};

	return (document.getElementById(
		'result_2'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${soNgayTrongThang()} ngày.</h5>`);
};

// Bài 3: Đọc số.
// Hàm đọc số.
var docSo = function () {
	// Tạo biến lấy giá trị
	var layGiaTri = document.getElementById('layGiaTri').value * 1;

	// Tạo các biến tách ra hàng trăm, hàng chục, hàng đơn vị.
	var hangTram = Math.floor(layGiaTri / 100);
	var hangChuc = Math.floor((layGiaTri % 100) / 10);
	var hangDonVi = Math.floor(layGiaTri % 10);

	// Kiểm tra dữ liệu nhập vào.
	if (layGiaTri < 100 || layGiaTri > 1000) {
		return alert('Dữ liệu nhập vào sai, vui lòng nhập lại.');
	}
	var docSoHangTram = function (n) {
		switch (n) {
			case 1:
				return 'Một';
			case 2:
				return 'Hai';
			case 3:
				return 'Ba';
			case 4:
				return 'Bốn';
			case 5:
				return 'Năm ';
			case 6:
				return 'Sáu';
			case 7:
				return 'Bảy';
			case 8:
				return 'Tám';
			case 9:
				return 'Chín';
		}
	};

	var docSoHangChuc = function (n) {
		switch (n) {
			case 0:
				if (hangDonVi == 0) {
					return '';
				}
				return 'lẻ';
			case 1:
				return 'mười';
			case 2:
				return 'hai mươi';
			case 3:
				return 'ba mươi';
			case 4:
				return 'bốn mươi';
			case 5:
				return 'năm mươi';
			case 6:
				return 'sáu mươi';
			case 7:
				return 'bảy mươi';
			case 8:
				return 'tám mươi';
			case 9:
				return 'chín mươi';
		}
	};

	var docSoHangDonVi = function (n) {
		switch (n) {
			case 0:
				return '';
			case 1:
				if (hangChuc > 1) {
					return 'mốt';
				}
				return 'một';
			case 2:
				return 'hai';
			case 3:
				return 'ba';
			case 4:
				return 'bốn';
			case 5:
				return 'năm';
			case 6:
				return 'sáu';
			case 7:
				return 'bảy';
			case 8:
				return 'tám';
			case 9:
				return 'chín';
		}
	};

	return (document.getElementById(
		'result_3'
	).innerHTML = `<h5 class="text__result"> 
	<img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${docSoHangTram(
		hangTram
	)} trăm ${docSoHangChuc(hangChuc)} ${docSoHangDonVi(hangDonVi)}</h5>`);
};

// Bài 4: Tìm sinh viên xa trường nhất.
var timSvXaTruongNhat = function () {
	// Lấy giá trị nhập vào từ form.
	// Sinh viên 1
	var tenSv_1 = document.getElementById('tenSv_1').value;
	var toaDoXSv_1 = document.getElementById('toaDoXsv_1').value * 1;
	var toaDoYSv_1 = document.getElementById('toaDoYsv_1').value * 1;

	// Sinh viên 2
	var tenSv_2 = document.getElementById('tenSv_2').value;
	var toaDoXSv_2 = document.getElementById('toaDoXsv_2').value * 1;
	var toaDoYSv_2 = document.getElementById('toaDoYsv_2').value * 1;

	// Sinh viên 3
	var tenSv_3 = document.getElementById('tenSv_3').value;
	var toaDoXSv_3 = document.getElementById('toaDoXsv_3').value * 1;
	var toaDoYSv_3 = document.getElementById('toaDoYsv_3').value * 1;

	// Trường học.
	var toaDoXTruong = document.getElementById('toaDoXTruongHoc').value * 1;
	var toaDoYTruong = document.getElementById('toaDoYTruongHoc').value * 1;

	// Hàm tính đoạn đường từ nhà sinh viên tới trường.
	var tuNhaSvToiTruong = function (toaDoXSv, toaDoYSv) {
		return Math.sqrt(
			((toaDoYTruong - toaDoXTruong) ^ 2) + ((toaDoYSv - toaDoXSv) ^ 2)
		);
	};

	// Hàm tính đoạn đường từ nhà sinh viên 1 tới trường.
	var tuNhaSv1ToiTruong = tuNhaSvToiTruong(toaDoXSv_1, toaDoYSv_1);

	// Hàm tính đoạn đường từ nhà sinh viên 2 tới trường.
	var tuNhaSv2ToiTruong = tuNhaSvToiTruong(toaDoXSv_2, toaDoYSv_2);

	// Hàm tính đoạn đường từ nhà sinh viên 3 tới trường.
	var tuNhaSv3ToiTruong = tuNhaSvToiTruong(toaDoXSv_3, toaDoYSv_3);

	// Hàm tính trường hợp đoạn đường từ nhà sinh viên 1 tới trường là lớn nhất.
	var sv1LonNhat = function()
};
console.log('timSvXaTruongNhat ~ timSvXaTruongNhat', timSvXaTruongNhat());
