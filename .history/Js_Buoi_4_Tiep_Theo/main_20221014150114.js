var result_1 = function () {
	var d = document.getElementById('date_1').value * 1;
	var m = document.getElementById('date_2').value * 1;
	var y = document.getElementById('date_3').value * 1;

	// var d = 12;
	// var m = 11;
	// var y = 2002;

	/* hàm kiểm tra năm nhuận */
	var nhuan = function () {
		return (y % 4 == 0 && y % 100 != 0) || y % 400 == 0;
	};

	/* hàm đếm số ngày trong tháng */
	function soNgayTrongThang() {
		switch (m) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12: {
				return 31;
			}

			case 2: {
				if (nhuan()) {
					return 29;
				}
				return 28;
			}
			case 4:
			case 6:
			case 9:
			case 11: {
				return 30;
			}
		}
	}

	// Hàm kiểm tra nhập số ngày tháng.
    if (d > soNgayTrongThang()){
        return alert('Bạn đã nhập sai ngày, vui lòng nhập lại.')
    } else if(m < 0 && m > 12)
    

	/* hàm kiểm tra xem ngày tiếp theo là ngày nào, hàm có 3 tham số là y, m, d*/
	function ngayTiepTheo() {
		//ta cần khai báo các biến ny, nm, nd là ngày tháng năm tiếp theo
		var ny = y;
		var nm = m;
		var nd = d;

		//nếu ngày tháng năm thỏa mãn điều kiện của nó (nghĩa là tháng năm phải lớn hơn 0,....)
		if ((y > 0 && m > 0 && m < 13 && d > 0) <= soNgayTrongThang()) {
			nd = d + 1;

			//nếu tháng nhập vào không phải tháng 12 và số ngày bằng số ngày tối đa của tháng thì ta tăng tháng lên 1 và ngày = 1
			if (m != 12 && d == soNgayTrongThang()) {
				nd = 1;
				nm = m + 1;
			}

			//nếu tháng nhập vào là tháng 12 và số ngày bằng số ngày bằng 31 thì ta tăng tháng, năm lên 1 và ngày sẽ bằng 1
			else if (m == 12 && d == soNgayTrongThang()) {
				nd = 1;
				ny = y + 1;
				nm = 1;
			} else if (m == 2) {
				//nếu tháng nhập vào là tháng 2 và năm nhuận thì ngày tối đa sẽ là 29
				if (nhuan()) {
					//nếu người dùng nhập vào ngày 29 thì ta tăng tháng lên 1 và ngày bằng 1
					if (d == 29) {
						nd = 1;
						nm = m + 1;
					}
				}

				//ngược lại nếu tháng 2 và không phải năm nhuận thì tháng 2 có 28 ngày
				else {
					//nếu người dùng nhập vào ngày 28 thì tăng tháng lên 1 và ngày bằng 1
					if (d == 28) {
						nd = 1;
						nm = m + 1;
					}
				}
			}
		}
		return `Ngày tiếp theo : ${nd}/${nm}/${ny}`;
	}

	var ngayTiepTheo = ngayTiepTheo();
	console.log('ngayTiepTheo', ngayTiepTheo);

	document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${ngayTiepTheo}</h5>`;
};
