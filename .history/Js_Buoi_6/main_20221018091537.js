// Bài 1: Tìm số nguyên dương nhỏ nhất.
var soNguyenDuongNhoNhat = function () {
	var total = function () {
		var sum = 0;
		for (var i = 0; sum < 10000; i++) {
			sum += i;
		}

		return Math.max(i - 1);
	};

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> Số nguyên dương nhỏ nhất: ${total()}</h5>`);
};

// Bài 2: Tính tổng.
var tinhTong = function () {
	var x = document.getElementById('soX').value * 1;
	console.log('soX: ', soX);
	var n = document.getElementById('soN').value * 1;
	console.log('soN: ', soN);

	// Hàm tính tổng các lũy thừa.
	var total = function () {
		// Tạo biến S
		var S = 0;

		// Tạo vòng lặp
		// Tạo biến i, i sẽ bằng 1, 2, 3, 4, 5, ..., n
		for (var i = 1; i <= n; i++) {
			// Tạo biến lũy thừa. VD: Lũy thừa bằng x, x^1, x^2, x^3, x^4,...
			luyThua = Math.pow(x, i);

			// S là tổng của các lũy thừa. VD: S = x + x^1 + x^2 ... x^n
			S += luyThua;
		}

		return S;
	};

	return (document.getElementById(
		'result_2'
	).innerHTML = `<h5 class="text__result">${total()}</h5>`);
};

// Bài 3: Tính giai thừa.
var tinhGiaiThua = function () {
	var soGiaiThuaToiDa = document.getElementById('soGiaiThua').value * 1;

	var result = function () {
		var mult = 1;
		for (var n = 1; n <= soGiaiThuaToiDa; n++) {
			mult *= n;
		}
		return mult;
	};

	return (document.getElementById(
		'result_3'
	).innerHTML = `<h5 class="text__result">${result()}</h5>`);
};

// Bài 4: Tạo thẻ div.
var taoTheDiv = function(){
	var newDiv = document.createElement('div')

}

function addElement() {
	// create a new div element
	const newDiv = document.createElement("div");
  
	// and give it some content
	const newContent = document.createTextNode("Hi there and greetings!");
  
	// add the text node to the newly created div
	newDiv.appendChild(newContent);
  
	// add the newly created element and its content into the DOM
	const currentDiv = document.getElementById("div1");
	document.body.insertBefore(newDiv, currentDiv);
	
	return (document.getElementById(
		'result_4'
	).innerHTML = `<h5 class="text__result">${newDiv}</h5>`);
  }
