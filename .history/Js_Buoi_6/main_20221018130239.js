// Bài 1: Tìm số nguyên dương nhỏ nhất.
var soNguyenDuongNhoNhat = function () {
	var total = function () {
		var sum = 0;
		for (var i = 0; sum < 10000; i++) {
			sum += i;
		}

		return Math.max(i - 1);
	};

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> Số nguyên dương nhỏ nhất: ${total()}</h5>`);
};

// Bài 2: Tính tổng.
var tinhTong = function () {
	var x = document.getElementById('soX').value * 1;
	console.log('soX: ', soX);
	var n = document.getElementById('soN').value * 1;
	console.log('soN: ', soN);

	// Hàm tính tổng các lũy thừa.
	var total = function () {
		// Tạo biến S
		var S = 0;

		// Tạo vòng lặp
		// Tạo biến i, i sẽ bằng 1, 2, 3, 4, 5, ..., n
		for (var i = 1; i <= n; i++) {
			// Tạo biến lũy thừa. VD: Lũy thừa bằng x, x^1, x^2, x^3, x^4,...
			luyThua = Math.pow(x, i);

			// S là tổng của các lũy thừa. VD: S = x + x^1 + x^2 ... x^n
			S += luyThua;
		}

		return S;
	};

	return (document.getElementById(
		'result_2'
	).innerHTML = `<h5 class="text__result">${total()}</h5>`);
};

// Bài 3: Tính giai thừa.
var tinhGiaiThua = function () {

	// Tạo biến là số giai thừa tối đa lấy từ giá trị nhập vào.
	var soGiaiThuaToiDa = document.getElementById('soGiaiThua').value * 1;

	// Hàm tính giá trị giai thừa.
	var result = function () {

		
		var mult = 1;
		for (var n = 1; n <= soGiaiThuaToiDa; n++) {
			mult *= n;
		}
		return mult;
	};

	return (document.getElementById(
		'result_3'
	).innerHTML = `<h5 class="text__result">${result()}</h5>`);
};

// Bài 4: Tạo thẻ div.

var taoTheDiv = function () {
	var div = document.createElement('p');
	div.style.width = '100px';
	div.style.height = '100px';
	div.style.background = 'red';
	div.style.color = 'white';
	div.innerHTML = 'Hello';

	console.log(div);
	var result_4 = document.getElementById('result_4');
	console.log(result_4);
	return result_4.appendChild(div);

	// return (document.getElementById(
	// 	'result_4'
	// ).innerHTML = `<h5 class="text__result">${addElement()}</h5>`);
};
console.log(taoTheDiv());
