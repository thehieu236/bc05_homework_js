// Bài 1: Tìm số nguyên dương nhỏ nhất.
var soNguyenDuongNhoNhat = function () {
	var total = function () {
		var sum = 0;
		for (var i = 0; sum < 10000; i++) {
			sum += i;
		}

		return Math.max(i - 1);
	};

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> Số nguyên dương nhỏ nhất: ${total()}</h5>`);
};

// Bài 2: Tính tổng.
var tinhTong = function () {
	var x = document.getElementById('soX').value * 1;
	console.log('soX: ', soX);
	var n = document.getElementById('soN').value * 1;
	console.log('soN: ', soN);

	// Hàm tính tổng các lũy thừa.
	var total = function () {
		// Tạo biến S
		var S = 0;

		// Tạo vòng lặp
		// Tạo biến i, i sẽ bằng 1, 2, 3, 4, 5, ..., n
		for (var i = 1; i <= n; i++) {
			// Tạo biến lũy thừa. VD: Lũy thừa bằng x, x^1, x^2, x^3, x^4,...
			luyThua = Math.pow(x, i);

			// S là tổng của các lũy thừa. VD: S = x + x^1 + x^2 ... x^n
			S += luyThua;
		}

		return S;
	};

	return (document.getElementById(
		'result_2'
	).innerHTML = `<h5 class="text__result">${total()}</h5>`);
};

// Bài 3: Tính giai thừa.
var tinhGiaiThua = function () {
	// Tạo biến là số giai thừa tối đa lấy từ giá trị nhập vào.
	var soGiaiThuaToiDa = document.getElementById('soGiaiThua').value * 1;

	// Hàm tính giá trị giai thừa.
	var result = function () {
		// Biến phép nhân
		var mult = 1;
		for (var n = 1; n <= soGiaiThuaToiDa; n++) {
			mult *= n;
		}
		return mult;
	};

	return (document.getElementById(
		'result_3'
	).innerHTML = `<h5 class="text__result">${result()}</h5>`);
};

// Bài 4: Tạo thẻ div.
var taoTheDiv = function () {
	var theDiv = function (color, chanLe) {
		var div = document.createElement('p');
		div.style.width = '100%';
		div.style.height = '10%';
		div.style.padding = '4px';
		div.style.borderRadius = '5px';
		div.style.background = '#ecf0f3';
		div.style.boxShadow = '5px 5px 10px #d0d0d0, -5px -5px 10px #ffffff';
		div.style.background = color;
		div.style.color = 'white';
		div.innerHTML = chanLe;

		return div;
	};

	// Vòng lặp tạo ra thẻ div, phân biệt nếu số lẻ sẽ là màu xanh và lẻ sẽ là màu đỏ..
	for (var i = 0; i <= 10; i++) {
		if (i % 2 == 0) {
			document
				.getElementById('result_4')
				.appendChild(theDiv('#4649FF', 'Div Lẻ'));
		} else {
			document
				.getElementById('result_4')
				.appendChild(theDiv('#F96666', 'Div Chẵn'));
		}
	}
};

// Bài 5: In số nguyên tố.
// var inSoNguyenTo = function () {
// 	function isprime(n) {
// 		//flag = 0 => không phải số nguyên tố
// 		//flag = 1 => số nguyên tố

// 		let flag = 1;

// 		if (n < 2)
// 			return (flag = 0); /*Số nhỏ hơn 2 không phải số nguyên tố => trả về 0*/

// 		/*Sử dụng vòng lặp while để kiểm tra có tồn tại ước số nào khác không*/
// 		let i = 2;
// 		while (i < n) {
// 			if (n % i == 0) {
// 				flag = 0;
// 				break; /*Chỉ cần tìm thấy 1 ước số là đủ và thoát vòng lặp*/
// 			}
// 			i++;
// 		}

// 		return flag;
// 	}

// 	var n = document.getElementById('soNguyenTo').value * 1;

// 	let i = 0,
// 		check = 0,
// 		result = '';
// 	while (i < n) {
// 		check = isprime(i);
// 		if (check == 1) result += i + ' ';
// 		++i;
// 	}

// 	return (document.getElementById(
// 		'result_5'
// 	).innerHTML = `<h5 class="text__result">${result}</h5>`);
// };

var inSoNguyenTo = function () {
	// Tạo biến n là số nhập vào.
	var n = 20;

	// Kiểm tra x có phải là số nguyên tố không.
	var kiemTraSoNguyenTo = function (soTruyenVao) {
		var flag = 1;
		// Nếu flag = 0 thì số truyền vào không phải là số nguyên tố.
		// Nếu flag = 1 thì số truyền vào là số nguyên tố.

		// Số bé hơn 2 không phài là số nguyên tố.
		if (soTruyenVao < 2) {
			return (flag = 0);
		}

		//Nếu số truyền vào chia hết cho số từ 2 đến căn bậc hai của chính nó thì không phải số nguyên tố, còn lại là số nguyên tố.
		for (var i = 2; i <= Math.sqrt(soTruyenVao); i++) {
			if (soTruyenVao % i == 0) {
				return (flag = 0);
			}
		}

		return flag;
	};

	// Tạo biến x là số nguyên tố cần tìm.
	// Tạo biến kiểm tra x.
	// Tạo biến in ra kết quả.
	var x = 0;
	var kiemTra = 0;
	var ketQua = '';

	// Tạo vòng lặp x từ 0 đến n. Kiểm tra x là số nguyên tố hay không.
	for (var i = 0; i <= n; i++) {
		x++;
		kiemTra = kiemTraSoNguyenTo(x);//Kiểm tra 

		if (kiemTra == 1) {
			ketQua += x + ', ';
		}
	}

	console.log('ketQua:', ketQua);
	// document.getElementById(
	// 	'result_5'
	// ).innerHTML = `<h5 class="text__result">${x}</h5>`;
};
console.log('inSoNguyenTo:', inSoNguyenTo());
