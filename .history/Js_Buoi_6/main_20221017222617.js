// Bài 1: Tìm số nguyên dương nhỏ nhất.
var soNguyenDuongNhoNhat = function () {
	var total = function () {
		var sum = 0;
		for (var i = 0; sum < 10000; i++) {
			sum += i;
		}

		return Math.max(i - 1);
	};

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> Số nguyên dương nhỏ nhất: ${total()}</h5>`);
};

// 
