/*
4. Validation

+ Tài khoản tối đa 4 - 6 ký số, không để trống
+ Tên nhân viên phải là chữ, không để trống
+ Email phải đúng định dạng, không để trống
+ mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt), không để trống
+ Ngày làm không để trống, định dạng mm/dd/yyyy
+ Lương cơ bản 1 000 000 - 20 000 000, không để trống
+ Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)
+ Số giờ làm trong tháng 80 - 200 giờ, không để trống
*/

function kiemTraTrung(username, dsnv) {
	var index = timViTri(username, dsnv);

	if (index !== -1) {
		showMessageErr(
			'tbTKNV',
			'block',
			'Tên tài khoản đã tồn tại, vui lòng nhập lại.'
		);
		return false;
	}
	showMessageErr('tbTKNV', 'none', '');

	return true;
}

function kiemTraRong(valueInput, idErr, messagerErr) {

	if (valueInput.length === 0) {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
	if (valueInput == 'Chọn chức vụ') {
		showMessageErr(idErr, 'block', messagerErr);
	} else {
		showMessageErr(idErr, 'none', '');
		return true;
	}
}

// + Tài khoản tối đa 4 - 6 ký số, không để trống
function kiemTraKyTuUsename(valueInput, idErr) {
	var reg = /^[a-z0-9_-]{4,6}$/;
	var isUsername = reg.test(valueInput);
	if (isUsername) {
		showMessageErr(idErr, '');
		return true;
	} else {
		showMessageErr(idErr, 'block', 'Vui lòng nhập từ 4 đến 6 ký tự, tên tài khoản không được viết hoa.');
		return false;
	}
}

// + Tên nhân viên phải là chữ, không để trống
function kiemTraTenNhanVien(valueInput, idErr, messagerErr) {

	// Tách value thành mảng.
	valueInput.split
	
}
