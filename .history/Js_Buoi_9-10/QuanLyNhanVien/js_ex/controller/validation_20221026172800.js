function kiemTraTrung(username, dsnv) {
    var index = timViTri(username, dsnv);
    
    if (index !== -1) {
		showMessageErr(
			'tbTKNV',
			'Tên tài khoản đã tồn tại, vui lòng nhập lại.'
        );
        return false
    }
    
    return true
}

function kiemTraRong(input, idErr, messagerErr) {
    if (input == 0) {
        showMessageErr(idErr, messagerErr)
    }
}
