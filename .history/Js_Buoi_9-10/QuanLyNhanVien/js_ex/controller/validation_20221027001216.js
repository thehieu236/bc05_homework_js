function kiemTraTrung(username, dsnv) {
	var index = timViTri(username, dsnv);

	if (index !== -1) {
		showMessageErr(
			'tbTKNV',
			'block',
			'Tên tài khoản đã tồn tại, vui lòng nhập lại.'
		);
		return false;
	}
	showMessageErr('tbTKNV', 'none', '');

	return true;
}

var getValueSelect = $('#chucvu').val();
function kiemTraRong(valueInput, idErr, messagerErr) {
	if (valueInput.length === 0) {
		showMessageErr(idErr, 'block', messagerErr);

		return false;
	}
	else if (getValueSelect == 'Chọn chức vụ') {
		showMessageErr(idErr, 'block', messagerErr);

		return false;
	} else {
		showMessageErr(idErr, 'none', messagerErr);
		return true;
	}
}
