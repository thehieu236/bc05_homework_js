/*
4. Validation

+ Tài khoản tối đa 4 - 6 ký số, không để trống
+ Tên nhân viên phải là chữ, không để trống
+ Email phải đúng định dạng, không để trống
+ mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt), không để trống
+ Ngày làm không để trống, định dạng mm/dd/yyyy
+ Lương cơ bản 1 000 000 - 20 000 000, không để trống
+ Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)
+ Số giờ làm trong tháng 80 - 200 giờ, không để trống
*/

arrIdThongBao = {
	username: 'tbTKNV',
	nameFull: 'tbTen',
	email: 'tbEmail',
	passWord: 'tbMatKhau',
	date: 'tbNgay',
	salary: 'tbLuongCB',
	workTime: 'tbGiolam',
};

function kiemTraTrung(username, dsnv) {
	var index = timViTri(username, dsnv);

	if (index !== -1) {
		showMessageErr(
			'tbTKNV',
			'block',
			'Tên tài khoản đã tồn tại, vui lòng nhập lại.'
		);
		return false;
	}
	showMessageErr('tbTKNV', 'none', '');

	return true;
}

function kiemTraRong(valueInput, idErr, messagerErr) {
	if (valueInput.length == ) {
		
	}
}

// function kiemTraDieuKien(nv) {
// 	var getValueSelect = $('#chucvu').val();
// 	var isValid = true;
// 	isValid = kiemTraTrung(nv.username, dsnv);
// 	isValid =
// 		isValid &&
// 		kiemTraRong(nv.username, 'tbTKNV', 'Vui lòng nhập tài khoản.') &
// 			kiemTraRong(nv.nameFull, 'tbTen', 'Vui lòng nhập họ và tên.') &
// 			kiemTraRong(nv.email, 'tbEmail', 'Vui lòng nhập email.') &
// 			kiemTraRong(nv.passWord, 'tbMatKhau', 'Vui lòng nhập mật khẩu.') &
// 			kiemTraRong(nv.date, 'tbNgay', 'Vui lòng nhập lại ngày.') &
// 			kiemTraRong(nv.salary, 'tbLuongCB', 'Vui lòng nhập lương.') &
// 			kiemTraRong(getValueSelect, 'tbChucVu', 'Vui lòng chọn chức vụ.') &
// 			kiemTraRong(nv.workTime, 'tbGiolam', 'Nhập giờ làm của bạn.');

// 	isValid =
// 		isValid &&
// 		kiemTraDieuKien(nv.username, 'tbTKNV', 'Tài khoản phải từ 4 đến 6 ký tự.') &
// 			kiemTraDieuKien(nv.nameFull, 'tbTen', 'Tên nhân viên phải là chữ.');

// 	return isValid;
// }
