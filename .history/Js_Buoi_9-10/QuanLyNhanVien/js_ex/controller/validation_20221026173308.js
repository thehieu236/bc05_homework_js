function kiemTraTrung(username, dsnv) {
	var index = timViTri(username, dsnv);

	if (index !== -1) {
		showMessageErr('tbTKNV', 'Tên tài khoản đã tồn tại, vui lòng nhập lại.');
		return false;
	}

	return true;
}

function kiemTraRong(valueInput, idErr, messagerErr) {
	if (valueInput.le == 0) {
		showMessageErr(idErr, messagerErr);
		return false;
	} else {
		return true;
	}
}
