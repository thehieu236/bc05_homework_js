//  DỰ ÁN QUẢN LÝ NHÂN VIÊN
/*
1. In ra table danh sách nhân viên

2. Thêm nhân viên mới

3. Tạo đối tượng nhân viên với thông tin lấy từ form người dùng nhập vào.

+ Đối tượng nhân viên bao gồm các thuộc tính sau:
+ Tài khoản
+ Họ tên
+ Email
+ Mật khẩu
+ Ngày làm
+ Lương cơ bản
+ Chức vụ gồm: Giám đốc, Trưởng Phòng, Nhân Viên
+ Giờ làm trong tháng
+ Tổng lương
+ Loại nhân viên

4. Validation

+ Tài khoản tối đa 4 - 6 ký số, không để trống
+ Tên nhân viên phải là chữ, không để trống
+ Email phải đúng định dạng, không để trống
+ mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt), không
để trống
+ Ngày làm không để trống, định dạng mm/dd/yyyy
+ Lương cơ bản 1 000 000 - 20 000 000, không để trống
+ Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)
+ Số giờ làm trong tháng 80 - 200 giờ, không để trống


5. Xây dựng phương thức tính tổng lương cho đối tượng nhân viên.

+ nếu chức vụ là giám đốc: tổng lương = lương cơ bản * 3.
+nếu chức vụ là trưởng phòng: tổng lương = lương cơ bản * 2.
+nếu chức vụ là nhân viên: tổng lương = lương cơ bản *.


6. Xây dựng phương thức xếp loại cho đối tượng nhân viên:


+ nếu nhân viên có giờ làm trên 192h(>= 192): nhân viên xuất sắc.
+ nếu nhân viên có giờ làm trên 176h (>=176): nhân viên giỏi.
+ nếu nhân viên có giờ làm trên 160h (>=160): nhân viên khá.
+ nếu nhân viên có giờ làm dưới 160h: nhân viên trung bình.


7. Xóa nhân viên
8. Cập nhật nhân viên (có validation)
9. Tìm Nhân Viên theo loại (xuất săc, giỏi, khá...) và hiển thị
*/

var DSNV = 'DSNV'
var dsnv = [];

var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
	var nvArr = JSON.parse(dataJson);

	for (let index = 0; index < <nav class="navbar navbar-expand-sm navbar-dark bg-primary">
		<a class="navbar-brand" href="#">Navbar</a>
		<button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
			aria-expanded="false" aria-label="Toggle navigation"></button>
		<div class="collapse navbar-collapse" id="collapsibleNavId">
			<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				<li class="nav-item active">
					<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Link</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
					<div class="dropdown-menu" aria-labelledby="dropdownId">
						<a class="dropdown-item" href="#">Action 1</a>
						<a class="dropdown-item" href="#">Action 2</a>
					</div>
				</li>
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<input class="form-control mr-sm-2" type="text" placeholder="Search">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
		</div>
	</nav>.length; index++) {
		var item = nvArr[index];
		var nv = new member(
			item.username,
			item.nameFull,
			item.email,
			item.passWord,
			item.date,
			item.salary,
			item.position,
			item.workTime
		);

		dsnv.push(nv);
	}
}

function luuLocalStorage() {
	let jsonDsnv = JSON.stringify(dsnv);
	localStorage.setItem('DSNV', jsonDsnv);
}

function themMember() {
	var nv = layThongTinNhanVien();
	var getValueSelect = $('#chucvu').val();
	var isValid = true;
	isValid = kiemTraTrung(nv.username, dsnv);
	isValid =
		isValid &&
		kiemTraRong(nv.username, 'tbTKNV', 'Vui lòng nhập tài khoản.') &
			kiemTraRong(nv.nameFull, 'tbTen', 'Vui lòng nhập họ và tên.') &
			kiemTraRong(nv.email, 'tbEmail', 'Vui lòng nhập email.') &
			kiemTraRong(nv.passWord, 'tbMatKhau', 'Vui lòng nhập mật khẩu.') &
			kiemTraRong(nv.date, 'tbNgay', 'Vui lòng nhập lại ngày.') &
			kiemTraRong(nv.salary, 'tbLuongCB', 'Vui lòng nhập lương.') &
			kiemTraRong(getValueSelect, 'tbChucVu', 'Vui lòng chọn chức vụ.') &
			kiemTraRong(nv.workTime, 'tbGiolam', 'Nhập giờ làm của bạn.');

	if (isValid) {
		dsnv.push(nv);

		luuLocalStorage();
		renderDsnv(dsnv);
	}
}

function capNhatThongTinNv() {
	console.log('object');
}
