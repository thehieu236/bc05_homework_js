// Bài 1:  Tính tiền thuế.
var tinhTienThue = function () {
	var hoVaTen = document.getElementById('hoVaTen').value;
	var tongThuNhapNam = document.getElementById('tongThuNhapNam').value * 1;
	var soNguoiPhuThuoc = document.getElementById('soNguoiPhụThuoc').value * 1;

	var thuNhapChiuThue = tongThuNhapNam - 4e6 - soNguoiPhuThuoc * 1.6e6;

	var tienThue = function () {
		if (thuNhapChiuThue < 0) {
			return alert('Dữ liệu sai, vui lòng nhập lại.');
		} else if (thuNhapChiuThue <= 60e6) {
			return thuNhapChiuThue * 0.05;
		} else if (thuNhapChiuThue <= 120e6) {
			return thuNhapChiuThue * 0.1;
		} else if (thuNhapChiuThue <= 210e6) {
			return thuNhapChiuThue * 1.5;
		} else if (thuNhapChiuThue <= 384e6) {
			return thuNhapChiuThue * 2;
		} else if (thuNhapChiuThue <= 6246) {
			return thuNhapChiuThue * 2.5;
		} else if (thuNhapChiuThue <= 9606) {
			return thuNhapChiuThue * 3;
		} else {
			return thuNhapChiuThue * 3.5;
		}
	};

	var formatTienThue = new Intl.NumberFormat('vn-VN').format(tienThue());

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> Họ tên: ${hoVaTen}. Tiền thuế thu nhập cá nhân: ${formatTienThue} VND</h5>`);
};

$('#lang').on('change', function () {
	$('#selected-value').text($(this).find(':selected').val());
	$('#selected-text').text($(this).find(':selected').text());
});

let select = document.querySelector('#lang');
let vl = document.getElementById('')
let result = document.querySelector('#result');
select.addEventListener('change', function () {
	result.innerHTML = ` <input type="number" id="diemThuHai" class="form-control input__form"
    placeholder="Nhập mã khách hàng">`;
});
