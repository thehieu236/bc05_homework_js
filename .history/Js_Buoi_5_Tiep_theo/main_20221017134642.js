// Bài 1:  Tính tiền thuế.
var tinhTienThue = function () {
	// Tạo biến lấy giá trị nhập vào.
	var hoVaTen = document.getElementById('hoVaTen').value;
	var tongThuNhapNam = document.getElementById('tongThuNhapNam').value * 1;
	var soNguoiPhuThuoc = document.getElementById('soNguoiPhụThuoc').value * 1;

	// Tạo biến tính thu nhập phải chịu thuế.
	var thuNhapChiuThue = tongThuNhapNam - 4e6 - soNguoiPhuThuoc * 1.6e6;

	// Hàm tính tiền thuế.
	var tienThue = function () {
		if (thuNhapChiuThue < 0) {
			return alert('Dữ liệu sai, vui lòng nhập lại.');
		} else if (thuNhapChiuThue <= 60e6) {
			return thuNhapChiuThue * 0.05;
		} else if (thuNhapChiuThue <= 120e6) {
			return thuNhapChiuThue * 0.1;
		} else if (thuNhapChiuThue <= 210e6) {
			return thuNhapChiuThue * 0.15;
		} else if (thuNhapChiuThue <= 384e6) {
			return thuNhapChiuThue * 0.2;
		} else if (thuNhapChiuThue <= 6246) {
			return thuNhapChiuThue * 0.25;
		} else if (thuNhapChiuThue <= 9606) {
			return thuNhapChiuThue * 0.3;
		} else {
			return thuNhapChiuThue * 0.35;
		}
	};

	// Biến Format về tiền VND.
	var formatTienThue = new Intl.NumberFormat('vn-VN').format(tienThue());

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> Họ tên: ${hoVaTen}. Tiền thuế thu nhập cá nhân: ${formatTienThue} VND</h5>`);
};

// Bài 2: Tính tiền cáp.
var tienCap = function () {
	
	// Hàm lấy giá trị nhập vào.
	var maKhachHang = document.getElementById('maKhachHang').value * 1;
	var soKenhCaoCap = document.getElementById('soKenhCaoCap').value * 1;
	var soKetNoi = document.getElementById('soKetNoi').value * 1

	var nhaDan = 0;
	var khachHang = 0;

	chonLoaiKhachHang(nhaDan, khachHang);
	if()
};

// Hàm chọn loại khách hàng.
function chonLoaiKhachHang(nhaDan, doanhNghiep) {
	var loaiKhachHang = document.getElementById('lang').value;
	switch (loaiKhachHang) {
		case 'Chọn loại khách hàng':
			return alert('Hãy chọn loại khách hàng');
		case 'doanhNghiep':
			result.innerHTML = ` <input type="number" id="soKetNoi" class="form-control input__form" placeholder="Số kết nối">`;
			break;

		default:
			result.innerHTML = ``;
	}

	switch (loaiKhachHang) {
		case 'Nhà dân':
			return (nhaDan = 0);
		case 'doanhNghiep':
			return (doanhNghiep = 1);
	}
}
