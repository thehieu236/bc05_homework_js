/*

Input: n1, n2, n3

Step:
 + Step 1: Các trường hợp từng số bé nhất:

    - n1 bé nhất.

	n1 < n2 và n1 < n3,

 	n2 < n3 => Output = n1, n2, n3
	n2 > n3 => Output = n1, n3, n2
	n2 = n3 => Output = n1, n2, n3

	- n2 bé nhất:

	n2 < n1 và n2 < n3,

	n1 < n3 => Output = n2, n1, n3
	n1 > n3 => Output = n2, n3, n1
	n1 = n3 => Output = n2, n1, n3

	- n3 bé nhất:

	n3 < n1 và n3 < n2,

	n2 < n1 => Output = n3, n2, n1
	n2 > n1 => Output = n3, n1, n2
	n2 = n1 => Output = n3, n2, n1

 + Step 2: Các trường hợp từng số lớn nhất:

	n1 > n2 và n1 > n3 => Output = n3, n2, n1
	n2 > n3 và n2 > n1 => Output = n1, n3, n2
	n3 > n2 và n3 > n1 => Output = n1, n2, n3

 + Step 3: Trường hợp 3 số bằng nhau:

	n1 = n2 = n3 => Output = n1, n2, n3


	*/

function result_1() {
	var n1 = document.getElementById('number_1').value * 1;
	var n2 = document.getElementById('number_2').value * 1;
	var n3 = document.getElementById('number_3').value * 1;

	//  + Step 1: Các trường hợp từng số bé nhất:

	//  - n1 bé nhất:
	if (n1 < n2 && n1 < n3) {
		if (n2 <= n3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n3}, ${n2}</h5>`;
		 }
		//  else if (n2 > n3) {
		// 	document.getElementById(
		// 		'result_1'
		// 	).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n2}, ${n3}</h5>`;
		// } 
		else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n}, ${n3}</h5>`;
			
		}
	}

	//  - n2 bé nhất:
	else if (n2 < n1 && n2 < n3) {
		if (n1 < n3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n2}, ${n1}, ${n3}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n2}, ${n3}, ${n1}</h5>`;
		}
	}

	// - n3 bé nhất:
	else if (n3 < n1 && n3 < n2) {
		if (n1 < n2) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n3}, ${n1}, ${n2}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n3}, ${n2}, ${n1}</h5>`;
		}
	}

	// Step 2: Trường hợp từng số lớn nhất.
	else {
		if (n1 > n2 && n1 > n3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n3}, ${n2}, ${n1}</h5>`;
		} else if (n2 > n1 && n2 > n3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n3}, ${n1}, ${n2}</h5>`;
		} else if (n3 > n1 && n3 > n2) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n1}, ${n3}</h5>`;
		}

		// Step 3: Trường hợp 3 số bằng nhau:
		else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n1}, ${n3}</h5>`;
		}
	}
}
