
// Input: n1, n2, n3

// Step: Các trường hợp từng số bé nhất:
//  + Step 1: n1 bé nhất:

// 	n1 < n2 và n1 < n3,

//  n2 < n3 => sắp xếp n1, n2, n3
// 	n2 > n3 => sắp xếp n1, n3, n2
// 	n2 = n3 => sắp xếp n1, n2, n3

// 	+ Step 2: n2 bé nhất:

// 	n2 < n1 và n2 < n3,

// 	n1 < n3 => sắp xếp n2, n1, n3
// 	n1 > n3 => sắp xếp n2, n3, n1
// 	n1 = n3 => sắp xếp n2, n1, n3

// 	+ Step 2: n3 bé nhất:

// 	n3 < n1 và n3 < n2,

// 	n2 < n1 => sắp xếp n3, n2, n1
// 	n2 > n1 => sắp xếp n3, n1, n2
// 	n2 = n1 => sắp xếp n3, n2, n1



function result_1() {
	var num1 = document.getElementById('number_1').value * 1;
	var num2 = document.getElementById('number_2').value * 1;
	var num3 = document.getElementById('number_3').value * 1;

//  + Step 1: n1 bé nhất:

	if (num1 < num2 && num1 < num3) {
		if (num2 < num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num2}, ${num3}</h5>`;
		} else if(num2 < num3){
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num3}, ${num2}</h5>`;
		} else {}
		
// 	+ Step 2: n2 bé nhất:

	} else if (num2 < num1 && num2 < num3) {
		if (num1 < num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num2}, ${num1}, ${num3}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num2}, ${num3}, ${num1}</h5>`;
		}
// 	+ Step 2: n3 bé nhất:

	} else if (num3 < num1 && num3 < num2) {
		if (num2 < num1) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num2}, ${num1}</h5>`;
		} else {
		}
	} else{
		document.getElementById(
			'result_1'
		).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num2}, ${num3}</h5>`;
	} 
}
