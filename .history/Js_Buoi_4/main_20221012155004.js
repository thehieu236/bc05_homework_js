/*

Input: num1, n2, n3

Step:
 + Step 1: Các trường hợp từng số bé nhất:

- num1 bé nhất.

	num1 < n2 và num1 < n3,

 n2 < n3 => sắp xếp num1, n2, n3
	n2 > n3 => sắp xếp num1, n3, n2
	n2 = n3 => sắp xếp num1, n2, n3

	- n2 bé nhất:

	n2 < num1 và n2 < n3,

	num1 < n3 => sắp xếp n2, num1, n3
	num1 > n3 => sắp xếp n2, n3, num1
	num1 = n3 => sắp xếp n2, num1, n3

	- n3 bé nhất:

	n3 < num1 và n3 < n2,

	n2 < num1 => sắp xếp n3, n2, num1
	n2 > num1 => sắp xếp n3, num1, n2
	n2 = num1 => sắp xếp n3, n2, num1

 + Step 2: Các trường hợp từng số lớn nhất:

	num1 > n2 và num1 > n3 => sắp xếp n3, n2, num1
	n2 > n3 và n2 > num1 => sắp xếp num1, n3, n2
	n3 > n2 và n3 > num1 => sắp xếp num1, n2, n3

	+ Step 3: Trường hợp 3 số bằng nhau:

	num1 = n2 = n3 => sắp xếp num1, n2, n3


	*/

function result_1() {
	var num1 = document.getElementById('number_1').value * 1;
	var num2 = document.getElementById('number_2').value * 1;
	var num3 = document.getElementById('number_3').value * 1;

	//  + Step 1: Các trường hợp từng số bé nhất:

	//  - num1 bé nhất:
	if (num1 < num2 && num1 < num3) {
		if (num2 < num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num3}, ${num2}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num2}, ${num3}</h5>`;
		}
	}

	//  - n2 bé nhất:
	else if (num2 < num1 && num2 < num3) {
		if (num1 < num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num2}, ${num1}, ${num3}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num2}, ${num3}, ${num1}</h5>`;
		}
	}

	// - n3 bé nhất:
	else if (num3 < num1 && num3 < num2) {
		if (num1 < num2) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num1}, ${num2}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num2}, ${num1}</h5>`;
		}
	}

	// Step 2: Trường hợp từng số lớn nhất.
	else {
		if (num1 > num2 && num1 > num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num2}, ${num1}</h5>`;
		} else if (num2 > num1 && num2 > num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num1}, ${num2}</h5>`;
		} else if (num3 > num1 && num3 > num2) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num1}, ${num3}</h5>`;
		}

		// Step 3: Trường hợp 3 số bằng nhau:
		else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num1}, ${num3}</h5>`;
		}
	}
}
