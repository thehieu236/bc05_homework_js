/* Bài 1: Xuất 3 số theo thứ tự tăng dần.

Input: n1, n2, n3

Step:
 + Step 1: Các trường hợp từng số bé nhất:

    - n1 bé nhất.

	n1 < n2 và n1 < n3,

 	n2 < n3 => Output = n1, n2, n3
	n2 > n3 => Output = n1, n3, n2
	n2 = n3 => Output = n1, n2, n3

	- n2 bé nhất:

	n2 < n1 và n2 < n3,

	n1 < n3 => Output = n2, n1, n3
	n1 > n3 => Output = n2, n3, n1
	n1 = n3 => Output = n2, n1, n3

	- n3 bé nhất:

	n3 < n1 và n3 < n2,

	n2 < n1 => Output = n3, n2, n1
	n2 > n1 => Output = n3, n1, n2
	n2 = n1 => Output = n3, n2, n1

 + Step 2: Các trường hợp từng số lớn nhất:

	n1 > n2 và n1 > n3 => Output = n3, n2, n1
	n2 > n3 và n2 > n1 => Output = n1, n3, n2
	n3 > n2 và n3 > n1 => Output = n1, n2, n3

 + Step 3: Trường hợp 3 số bằng nhau:

	n1 = n2 = n3 => Output = n1, n2, n3


	*/

function result_1() {
	var n1 = document.getElementById('number_1').value * 1;
	var n2 = document.getElementById('number_2').value * 1;
	var n3 = document.getElementById('number_3').value * 1;

	// Kiểm tra số nguyên.
	if (Math.ceil(n1) !== Math.floor(n1)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	if (Math.ceil(n2) !== Math.floor(n2)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	if (Math.ceil(n3) !== Math.floor(n3)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	//  + Step 1: Các trường hợp từng số bé nhất:

	//  - n1 bé nhất:
	if (n1 < n2 && n1 < n3) {
		if (n2 < n3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n2}, ${n3}</h5>`;
		} else if (n2 > n3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n3}, ${n2}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n2}, ${n3}</h5>`;
		}
	}

	//  - n2 bé nhất:
	else if (n2 < n1 && n2 < n3) {
		if (n1 < n3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n2}, ${n1}, ${n3}</h5>`;
		} else if (n1 > n3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n2}, ${n3}, ${n1}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n2}, ${n1}, ${n3}</h5>`;
		}
	}

	// - n3 bé nhất:
	else if (n3 < n1 && n3 < n2) {
		if (n2 < n1) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n3}, ${n2}, ${n1}</h5>`;
		} else if (n2 > n1) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n3}, ${n1}, ${n2}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n3}, ${n2}, ${n1}</h5>`;
		}
	}

	// Step 2: Trường hợp từng số lớn nhất.
	else {
		if (n1 > n2 && n1 > n3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n3}, ${n2}, ${n1}</h5>`;
		} else if (n2 > n1 && n2 > n3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n3}, ${n1}, ${n2}</h5>`;
		} else if (n3 > n1 && n3 > n2) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n1}, ${n3}</h5>`;
		}

		// Step 3: Trường hợp 3 số bằng nhau:
		else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n1}, ${n3}</h5>`;
		}
	}
}

// Bài 2:  Chương trình "Chào hỏi".
/**
 * Input: Selector
 * Step:
 *  + Step 1: Tạo function trả vế giá trị string cho từng value selector
 * 	+ Step 2: Tạo function lấy value của các option
 *
 * Output: Xuất ra kết quả.
 */

const thanhVien = '';
const bo = 'B';
const me = 'M';
const anhTrai = 'A';
const emGai = 'E';

function chonThanhVien(member) {
	/*
	if (member == thanhVien) {
		return 'Người lạ ơi!';
	} else if (member == bo) {
		return 'Bố!';
	} else if (member == me) {
		return 'Mẹ!';
	} else if (member == anhTrai) {
		return 'Anh trai!';
	} else if (member == emGai) {
		return 'Em gái!';
	}

*/

	switch (member) {
		case thanhVien:
			return 'Người lạ ơi!';
		case bo:
			return 'Bố!';
		case me:
			return 'Mẹ!';
		case anhTrai:
			return 'Anh trai!';
		case emGai:
			return 'Em gái!';
	}
}

function result_2() {
	var mem = $('#member option:selected').val();

	document.getElementById(
		'result_2'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${chonThanhVien(
		mem
	)}</h5>`;
}

//  Bài 3: Đếm số chẵn lẻ
function result_3() {
	var n1 = document.getElementById('exchange_1').value * 1;
	var n2 = document.getElementById('exchange_2').value * 1;
	var n3 = document.getElementById('exchange_3').value * 1;

	// Kiểm tra số nguyên.
	if (Math.ceil(n1) !== Math.floor(n1)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	if (Math.ceil(n2) !== Math.floor(n2)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	if (Math.ceil(n3) !== Math.floor(n3)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	// Kiểm tra chẵn, lẻ
	var a = n1 % 2 === 0 ? 0 : 1
	console.log("chanLe ~ a", a)
	var b = n2 % 2 === 0 ? 0 : 1
	console.log("chanLe ~ a", b)
	var c = n3 % 2 === 0 ? 0 : 1
	console.log("chanLe ~ a", c)

	// Đếm số chẵn, lẻ

	if((a + b + c) == 0 ){
		console.log("chan");
	}




}
