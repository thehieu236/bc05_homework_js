/*

Input: n1, num2, num3

Step:
 + Step 1: Các trường hợp từng số bé nhất:

- n1 bé nhất.

	n1 < num2 và n1 < num3,

 num2 < num3 => Output = n1, num2, num3
	num2 > num3 => Output = n1, num3, num2
	num2 = num3 => Output = n1, num2, num3

	- num2 bé nhất:

	num2 < n1 và num2 < num3,

	n1 < num3 => Output = num2, n1, num3
	n1 > num3 => Output = num2, num3, n1
	n1 = num3 => Output = num2, n1, num3

	- num3 bé nhất:

	num3 < n1 và num3 < num2,

	num2 < n1 => Output = num3, num2, n1
	num2 > n1 => Output = num3, n1, num2
	num2 = n1 => Output = num3, num2, n1

 + Step 2: Các trường hợp từng số lớn nhất:

	n1 > num2 và n1 > num3 => Output = num3, num2, n1
	num2 > num3 và num2 > n1 => Output = n1, num3, num2
	num3 > num2 và num3 > n1 => Output = n1, num2, num3

	+ Step 3: Trường hợp 3 số bằng nhau:

	n1 = num2 = num3 => Output = n1, num2, num3


	*/

function result_1() {
	var n1 = document.getElementById('number_1').value * 1;
	var num2 = document.getElementById('number_2').value * 1;
	var num3 = document.getElementById('number_3').value * 1;

	//  + Step 1: Các trường hợp từng số bé nhất:

	//  - n1 bé nhất:
	if (n1 < num2 && n1 < num3) {
		if (num2 < num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${num3}, ${num2}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${num2}, ${num3}</h5>`;
		}
	}

	//  - num2 bé nhất:
	else if (num2 < n1 && num2 < num3) {
		if (n1 < num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num2}, ${n1}, ${num3}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num2}, ${num3}, ${n1}</h5>`;
		}
	}

	// - num3 bé nhất:
	else if (num3 < n1 && num3 < num2) {
		if (n1 < num2) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${n1}, ${num2}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num2}, ${n1}</h5>`;
		}
	}

	// Step 2: Trường hợp từng số lớn nhất.
	else {
		if (n1 > num2 && n1 > num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num2}, ${n1}</h5>`;
		} else if (num2 > n1 && num2 > num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${n1}, ${num2}</h5>`;
		} else if (num3 > n1 && num3 > num2) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n1}, ${num3}</h5>`;
		}

		// Step 3: Trường hợp 3 số bằng nhau:
		else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${n1}, ${n1}, ${num3}</h5>`;
		}
	}
}
