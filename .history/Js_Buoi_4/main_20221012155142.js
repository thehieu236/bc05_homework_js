/*

Input: num1, num2, num3

Step:
 + Step 1: Các trường hợp từng số bé nhất:

- num1 bé nhất.

	num1 < num2 và num1 < num3,

 num2 < num3 => Output = num1, num2, num3
	num2 > num3 => Output = num1, num3, num2
	num2 = num3 => Output = num1, num2, num3

	- num2 bé nhất:

	num2 < num1 và num2 < num3,

	num1 < num3 => Output = num2, num1, num3
	num1 > num3 => Output = num2, num3, num1
	num1 = num3 => Output = num2, num1, num3

	- num3 bé nhất:

	num3 < num1 và num3 < num2,

	num2 < num1 => Output = num3, num2, num1
	num2 > num1 => Output = num3, num1, num2
	num2 = num1 => Output = num3, num2, num1

 + Step 2: Các trường hợp từng số lớn nhất:

	num1 > num2 và num1 > num3 => Output = num3, num2, num1
	num2 > num3 và num2 > num1 => Output = num1, num3, num2
	num3 > num2 và num3 > num1 => Output = num1, num2, num3

	+ Step 3: Trường hợp 3 số bằng nhau:

	num1 = num2 = num3 => Output = num1, num2, num3


	*/

function result_1() {
	var num1 = document.getElementById('number_1').value * 1;
	var num2 = document.getElementById('number_2').value * 1;
	var num3 = document.getElementById('number_3').value * 1;

	//  + Step 1: Các trường hợp từng số bé nhất:

	//  - num1 bé nhất:
	if (num1 < num2 && num1 < num3) {
		if (num2 < num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num3}, ${num2}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num2}, ${num3}</h5>`;
		}
	}

	//  - num2 bé nhất:
	else if (num2 < num1 && num2 < num3) {
		if (num1 < num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num2}, ${num1}, ${num3}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num2}, ${num3}, ${num1}</h5>`;
		}
	}

	// - num3 bé nhất:
	else if (num3 < num1 && num3 < num2) {
		if (num1 < num2) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num1}, ${num2}</h5>`;
		} else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num2}, ${num1}</h5>`;
		}
	}

	// Step 2: Trường hợp từng số lớn nhất.
	else {
		if (num1 > num2 && num1 > num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num2}, ${num1}</h5>`;
		} else if (num2 > num1 && num2 > num3) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num3}, ${num1}, ${num2}</h5>`;
		} else if (num3 > num1 && num3 > num2) {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num1}, ${num3}</h5>`;
		}

		// Step 3: Trường hợp 3 số bằng nhau:
		else {
			document.getElementById(
				'result_1'
			).innerHTML = `<h5 class="text__USD"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${num1}, ${num1}, ${num3}</h5>`;
		}
	}
}
