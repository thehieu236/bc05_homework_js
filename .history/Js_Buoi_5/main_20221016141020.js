// Bài 1:  Quản lý tuyển sinh
var traDiem = function () {
	var diemChuan = document.getElementById('diemChuan').value * 1;
	var diemMonThuNhat = document.getElementById('diemThuNhat').value * 1;
	var diemMonThuHai = document.getElementById('diemThuHai').value * 1;
	var diemMonThuBa = document.getElementById('diemThuBa').value * 1;

	// Hàm lấy giá trị từ chọn khu vực.
	var chonKhuVuc = function () {
		var selectedVal = $('#chonKhuVuc option:selected').val();
		return selectedVal;
	};

	// Điểm cộng cho mỗi khu vực, nếu không thuộc khu vực nào thì không có điểm cộng.
	var diemCongKV = function () {
		switch (chonKhuVuc()) {
			case 'A':
				return 2;
			case 'B':
				return 1;
			case 'C':
				return 0.5;
		}
		return 0;
	};

	// Hàm lấy giá trị từ chọn đối tượng.
	var chonDoiTuong = function () {
		var selectedVal = $('#chonDoiTuong option:selected').val() * 1;
		return selectedVal;
	};

	// Điểm cộng cho mỗi đối tượng, nếu không thuộc đối tượng nào thì không có điểm cộng.
	var diemCongDT = function () {
		switch (chonDoiTuong()) {
			case 1:
				return 2.5;
			case 2:
				return 1.5;
			case 3:
				return 1;
		}
		return 0;
	};

	// Hàm tính tổng ba môn và điểm cộng.
	var total = function () {
		var sum =
			diemMonThuNhat +
			diemMonThuHai +
			diemMonThuBa +
			diemCongKV() +
			diemCongDT();
		return sum;
	};

	// Kiểm tra tổng ba môn nếu cao hơn điểm chuẩn thì đậu, thấp hơn thì rớt.
	var result = function () {
		// Kiểm tra nếu 1 trong 3 môn bị điểm 0 thì cho rớt luôn.
		switch (0) {
			case diemMonThuNhat:
				return `rớt`;
			case diemMonThuHai:
				return `rớt`;
			case diemMonThuBa:
				return `rớt`;
		}

		if (total() >= diemChuan) {
			return 'Đậu';
		} else {
			return 'Rớt';
		}
	};

	return (document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> Bạn đã ${result()}. Tổng điểm: ${total()}</h5>`);
};

// Bài 2: Tính ngày.
var tinhTienDien = function () {
	var hoVaTen = document.getElementById('tinhTienDien').value;
	var soKw = document.getElementById('soKw').value * 1;

	// Hàm tính giá điện.
	var giaDien = function () {
		if (soKw < 0) {
			return alert('Dữ liệu nhập vào sai, vui lòng nhập lại.');

			// 50kw đầu.
		} else if (soKw < 50) {
			return 500;

			// 50kw kế.
		} else if (soKw < 100) {
			return 650;

			// 100kw kế.
		} else if (soKw < 200) {
			return 850;

			// 150kw kế.
		} else if (soKw < 350) {
			return 1100;

			// Còn lại.
		} else {
			return 1300;
		}
	};

	// Hàm tính tiền điện.
	var tienDien = function () {
		if (soKw) {
		}
	};

	var tienDien50KwDau = soKw * 500;
	var tienDien50KwKe = (soKw % 50) * 650;
	var tienDien100KwKe =  (soKw % 100)  * 850;
	var tienDienw150KwKe =  (soKw % 150)  * 110;
};
