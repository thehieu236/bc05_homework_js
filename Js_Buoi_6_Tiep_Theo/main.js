// Bài 5: In số nguyên tố.

var inSoNguyenTo = function () {
	// Tạo biến n là số nhập vào.
	var n = document.getElementById('soNguyenTo').value * 1;

	// Kiểm tra x có phải là số nguyên tố không.
	var kiemTraSoNguyenTo = function (soTruyenVao) {
		var flag = 1;
		// Nếu flag = 0 thì số truyền vào không phải là số nguyên tố.
		// Nếu flag = 1 thì số truyền vào là số nguyên tố.

		// Số bé hơn 2 không phài là số nguyên tố.
		if (soTruyenVao < 2) {
			return (flag = 0);
		}

		//Nếu số truyền vào chia hết cho số từ 2 đến căn bậc hai của chính nó thì không phải số nguyên tố, còn lại là số nguyên tố.
		for (var i = 2; i <= Math.sqrt(soTruyenVao); i++) {
			if (soTruyenVao % i == 0) {
				return (flag = 0);
			}
		}

		return flag;
	};

	// Tạo biến x là số nguyên tố cần tìm.
	// Tạo biến kiểm tra x.
	// Tạo biến in ra kết quả.
	var x = 0;
	var kiemTra = 0;
	var ketQua = '';

	// Tạo vòng lặp x từ 0 đến n. Kiểm tra x là số nguyên tố hay không.
	for (var i = 0; i < n; i++) {
		x++;
		kiemTra = kiemTraSoNguyenTo(x); //Kiểm tra x có phải số nguyên tố không, nếu kết quả trả về 1 thì x là số nguyên tố.
		if (kiemTra == 1) {
			ketQua += x + ' ';
		}
	}

	return (document.getElementById(
		'result_5'
	).innerHTML = `<h5 class="text__result">${ketQua}</h5>`);
};
