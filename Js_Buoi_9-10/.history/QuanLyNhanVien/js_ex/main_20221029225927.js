//  DỰ ÁN QUẢN LÝ NHÂN VIÊN
/*
1. In ra table danh sách nhân viên

2. Thêm nhân viên mới

3. Tạo đối tượng nhân viên với thông tin lấy từ form người dùng nhập vào.

+ Đối tượng nhân viên bao gồm các thuộc tính sau:
+ Tài khoản
+ Họ tên
+ Email
+ Mật khẩu
+ Ngày làm
+ Lương cơ bản
+ Chức vụ gồm: Giám đốc, Trưởng Phòng, Nhân Viên
+ Giờ làm trong tháng
+ Tổng lương
+ Loại nhân viên

4. Validation

+ Tài khoản tối đa 4 - 6 ký số, không để trống
+ Tên nhân viên phải là chữ, không để trống
+ Email phải đúng định dạng, không để trống
+ mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt), không để trống
+ Ngày làm không để trống, định dạng mm/dd/yyyy
+ Lương cơ bản 1 000 000 - 20 000 000, không để trống
+ Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)
+ Số giờ làm trong tháng 80 - 200 giờ, không để trống

5. Xây dựng phương thức tính tổng lương cho đối tượng nhân viên.

+ nếu chức vụ là giám đốc: tổng lương = lương cơ bản * 3.
+nếu chức vụ là trưởng phòng: tổng lương = lương cơ bản * 2.
+nếu chức vụ là nhân viên: tổng lương = lương cơ bản *.

6. Xây dựng phương thức xếp loại cho đối tượng nhân viên:

+ nếu nhân viên có giờ làm trên 192h(>= 192): nhân viên xuất sắc.
+ nếu nhân viên có giờ làm trên 176h (>=176): nhân viên giỏi.
+ nếu nhân viên có giờ làm trên 160h (>=160): nhân viên khá.
+ nếu nhân viên có giờ làm dưới 160h: nhân viên trung bình.


7. Xóa nhân viên
8. Cập nhật nhân viên (có validation)
9. Tìm Nhân Viên theo loại (xuất săc, giỏi, khá...) và hiển thị
*/

var DSNV = 'DSNV';
var dsnv = [];
console.log('file: main.js ~ line 53 ~ dsnv', dsnv);

var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
	var nvArr = JSON.parse(dataJson);

	for (let index = 0; index < nvArr.length; index++) {
		var item = nvArr[index];
		var nv = new member(
			item.username,
			item.nameFull,
			item.email,
			item.passWord,
			item.date,
			item.salary,
			item.position,
			item.workTime
		);
		dsnv.push(nv);
	}

	renderDsnv(dsnv);
}

function luuLocalStorage() {
	let jsonDsnv = JSON.stringify(dsnv);
	localStorage.setItem('DSNV', jsonDsnv);
}

function suaThongTin(username) {
	var viTri = timViTri(username, dsnv);

	if (viTri == -1) {
		return;
	}
	var data = dsnv[viTri];
	showThongTinForm(data);

	document.getElementById('tknv').disabled = true;
}

function xoaThongTin(username) {
	var viTri = timViTri(username, dsnv);
	if (viTri !== -1) {
		dsnv.splice(viTri, 1);

		luuLocalStorage();
		renderDsnv(dsnv);
	}
}

function capNhatThongTinNv() {
	var data = layThongTinNhanVien();
	var viTri = timViTri(data.username, dsnv);
	if (viTri == -1) return;

	dsnv[viTri] = data;

	renderDsnv(dsnv);
	luuLocalStorage();

	// remove disable
	document.getElementById('tknv').disabled = false;
	resetForm();
}

function resetClose() {
	// remove disable
	document.getElementById('tknv').disabled = false;
	resetForm();
}

function themMember() {
	var nv = layThongTinNhanVien();
	var isValid =
		kiemTraRong(nv.username, 'tbTKNV', 'Vui lòng nhập tài khoản.') &
		kiemTraRong(nv.nameFull, 'tbTen', 'Vui lòng nhập họ và tên.') &
		kiemTraRong(nv.email, 'tbEmail', 'Vui lòng nhập email.') &
		kiemTraRong(nv.passWord, 'tbMatKhau', 'Vui lòng nhập mật khẩu.') &
		kiemTraRong(nv.date, 'tbNgay', 'Vui lòng nhập lại ngày.') &
		kiemTraRong(nv.salary, 'tbLuongCB', 'Vui lòng nhập lương.') &
		kiemTraRong(nv.workTime, 'tbGiolam', 'Nhập giờ làm của bạn.');

	isValid =
		isValid &&
		kiemTraKyTuUsename(nv.username, 'tbTKNV') &&
		kiemTraTrung(nv.username, dsnv) &
			kiemTraTenNhanVien(nv.nameFull, 'tbTen', 'Tên nhân viên phải là chữ.') &
			kiemTraEmail(nv.email, 'tbEmail', 'Vui lòng nhập đúng email.') &
			kiemTraPassWord(
				nv.passWord,
				'tbMatKhau',
				'Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt.)'
			) &
			kiemTraNgay(
				nv.date,
				'tbNgay',
				'Vui lòng nhập đúng định dạng(DD/MM/YYYY)'
			) &
			kiemTraMucLuong(
				nv.salary,
				'tbLuongCB',
				'Mức lương từ 1,000,000 - 20,000,000.'
			) &
			kiemTraChucVu(nv.position, 'tbChucVu', 'Vui lòng chọn chức vụ.') &
			kiemTraGioLam(nv.workTime, 'tbGiolam', 'Giờ làm phải từ 80 đến 200 giờ,');

	if (isValid == true) {
		dsnv.push(nv);

		luuLocalStorage();
		renderDsnv(dsnv);
	}

	console.log(Number());
}

function tinhTongLuong() {
	var nv = layThongTinNhanVien();
	var chucVu = nv.position;
	var luongCB = nv.salary;
	var luongSep = luongCB * 3;
	var luongTruongPhong = luongCB * 2;
	if (chucVu == 'Sếp') {
		return luongSep;
	} else if (chucVu == 'Trưởng phòng') {
		return luongTruongPhong;
	} else {
		return luongCB;
	}
}

var tt = tinhTongLuong();

var tongLuong = formatCurrency(tt);
console.log('file: main.js ~ line 184 ~ tongLuong', tongLuong);
