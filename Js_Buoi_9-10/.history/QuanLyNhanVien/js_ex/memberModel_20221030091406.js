function member(
	username,
	nameFull,
	email,
	passWord,
	date,
	salary,
	position,
	workTime
) {
	this.username = username;
	this.nameFull = nameFull;
	this.email = email;
	this.passWord = passWord;
	this.date = date;
	this.salary = salary;
	this.position = position;
	this.workTime = workTime;
	this.sumSalary = function () {
		luongCB = formatToNumber(salary);
		// Tính tổng lương theo chức vụ.
		var luongSep = luongCB * 3;
		var luongTruongPhong = luongCB * 2;

		if (position == 'Sếp') {
			return luongSep;
		} else if (position == 'Trưởng phòng') {
			return luongTruongPhong;
		} else {
			return luongCB;
		}
	};
	this.rank = function () {
		if (workTime >= 192) {
			return 'Xuất sắc';
		} else if (workTime >= 176) {
			return 'Giỏi';
		} else if (workTime >= 160) {
			return 'Nhân viên khá.';
		} else {
			return 'Nhân viên trung bình.';
		}
	};
}
