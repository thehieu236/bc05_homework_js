//  DỰ ÁN QUẢN LÝ NHÂN VIÊN
/*
1. In ra table danh sách nhân viên

2. Thêm nhân viên mới

3. Tạo đối tượng nhân viên với thông tin lấy từ form người dùng nhập vào.

+ Đối tượng nhân viên bao gồm các thuộc tính sau:
+ Tài khoản
+ Họ tên
+ Email
+ Mật khẩu
+ Ngày làm
+ Lương cơ bản
+ Chức vụ gồm: Giám đốc, Trưởng Phòng, Nhân Viên
+ Giờ làm trong tháng
+ Tổng lương
+ Loại nhân viên

4. Validation

+ Tài khoản tối đa 4 - 6 ký số, không để trống
+ Tên nhân viên phải là chữ, không để trống
+ Email phải đúng định dạng, không để trống
+ mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt), không để trống
+ Ngày làm không để trống, định dạng mm/dd/yyyy
+ Lương cơ bản 1 000 000 - 20 000 000, không để trống
+ Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)
+ Số giờ làm trong tháng 80 - 200 giờ, không để trống

5. Xây dựng phương thức tính tổng lương cho đối tượng nhân viên.

+ nếu chức vụ là giám đốc: tổng lương = lương cơ bản * 3.
+nếu chức vụ là trưởng phòng: tổng lương = lương cơ bản * 2.
+nếu chức vụ là nhân viên: tổng lương = lương cơ bản *.

6. Xây dựng phương thức xếp loại cho đối tượng nhân viên:

+ nếu nhân viên có giờ làm trên 192h(>= 192): nhân viên xuất sắc.
+ nếu nhân viên có giờ làm trên 176h (>=176): nhân viên giỏi.
+ nếu nhân viên có giờ làm trên 160h (>=160): nhân viên khá.
+ nếu nhân viên có giờ làm dưới 160h: nhân viên trung bình.


7. Xóa nhân viên
8. Cập nhật nhân viên (có validation)
9. Tìm Nhân Viên theo loại (xuất săc, giỏi, khá...) và hiển thị
*/

var DSNV = 'DSNV';
var dsnv = [];
console.log('file: main.js ~ line 53 ~ dsnv', dsnv);

var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
	var nvArr = JSON.parse(dataJson);

	for (let index = 0; index < nvArr.length; index++) {
		var item = nvArr[index];
		var nv = new member(
			item.username,
			item.nameFull,
			item.email,
			item.passWord,
			item.date,
			item.salary,
			item.position,
			item.workTime
		);
		dsnv.push(nv);
	}

	renderDsnv(dsnv);
}

function luuLocalStorage() {
	let jsonDsnv = JSON.stringify(dsnv);
	localStorage.setItem('DSNV', jsonDsnv);
}

function suaThongTin(username) {
	var viTri = timViTri(username, dsnv);

	if (viTri == -1) {
		return;
	}
	var data = dsnv[viTri];
	showThongTinForm(data);

	document.getElementById('tknv').disabled = true;
}

function xoaThongTin(username) {
	var viTri = timViTri(username, dsnv);
	if (viTri !== -1) {
		dsnv.splice(viTri, 1);

		luuLocalStorage();
		renderDsnv(dsnv);
	}
}

function capNhatThongTinNv() {
	var data = layThongTinNhanVien();
	var viTri = timViTri(data.username, dsnv);
	if (viTri == -1) return;

	dsnv[viTri] = data;

	renderDsnv(dsnv);
	luuLocalStorage();

	// remove disable
	document.getElementById('tknv').disabled = false;
	resetForm();
}

function resetClose() {
	// remove disable
	document.getElementById('tknv').disabled = false;
	resetForm();
}

function themMember() {
	var nv = layThongTinNhanVien();
	var isValid =
		kiemTraRong(nv.username, 'tbTKNV', 'Vui lòng nhập tài khoản.') &
		kiemTraRong(nv.nameFull, 'tbTen', 'Vui lòng nhập họ và tên.') &
		kiemTraRong(nv.email, 'tbEmail', 'Vui lòng nhập email.') &
		kiemTraRong(nv.passWord, 'tbMatKhau', 'Vui lòng nhập mật khẩu.') &
		kiemTraRong(nv.date, 'tbNgay', 'Vui lòng nhập lại ngày.') &
		kiemTraRong(nv.salary, 'tbLuongCB', 'Vui lòng nhập lương.') &
		kiemTraRong(nv.workTime, 'tbGiolam', 'Nhập giờ làm của bạn.');

	isValid =
		isValid &&
		kiemTraKyTuUsename(nv.username, 'tbTKNV') &&
		kiemTraTrung(nv.username, dsnv) &
			kiemTraTenNhanVien(nv.nameFull, 'tbTen', 'Tên nhân viên phải là chữ.') &
			kiemTraEmail(nv.email, 'tbEmail', 'Vui lòng nhập đúng email.') &
			kiemTraPassWord(
				nv.passWord,
				'tbMatKhau',
				'Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt.)'
			) &
			kiemTraNgay(
				nv.date,
				'tbNgay',
				'Vui lòng nhập đúng định dạng(DD/MM/YYYY)'
			) &
			kiemTraMucLuong(
				nv.salary,
				'tbLuongCB',
				'Mức lương từ 1,000,000 - 20,000,000.'
			) &
			kiemTraChucVu(nv.position, 'tbChucVu', 'Vui lòng chọn chức vụ.') &
			kiemTraGioLam(nv.workTime, 'tbGiolam', 'Giờ làm phải từ 80 đến 200 giờ,');

	if (isValid == true) {
		dsnv.push(nv);

		luuLocalStorage();
		renderDsnv(dsnv);
	}
}

function tinhTongLuong() {
	var nv = layThongTinNhanVien();
	var chucVu = nv.position;
	var luongCB = nv.salary;
	var luongSep = luongCB * 3;
	var luongTruongPhong = luongCB * 2;
	if (chucVu == 'Sếp') {
		return luongSep;
	} else if (chucVu == 'Trưởng phòng') {
		return luongTruongPhong;
	} else {
		return luongCB;
	}
}

// Jquery Dependency

$("input[data-type='currency']").on({
	keyup: function () {
		formatCurrency($(this));
	},
	blur: function () {
		formatCurrency($(this), 'blur');
	},
});

function formatNumber(n) {
	// format number 1000000 to 1,234,567
	return n.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function formatCurrency(input, blur) {
	// appends $ to value, validates decimal side
	// and puts cursor back in right position.

	// get input value
	var input_val = input.val();

	// don't validate empty input
	if (input_val === '') {
		return;
	}

	// original length
	var original_len = input_val.length;

	// initial caret position
	var caret_pos = input.prop('selectionStart');

	// check for decimal
	if (input_val.indexOf('.') >= 0) {
		// get position of first decimal
		// this prevents multiple decimals from
		// being entered
		var decimal_pos = input_val.indexOf('.');

		// split number by decimal point
		var left_side = input_val.substring(0, decimal_pos);
		var right_side = input_val.substring(decimal_pos);

		// add commas to left side of number
		left_side = formatNumber(left_side);

		// validate right side
		right_side = formatNumber(right_side);

		// On blur make sure 2 numbers after decimal
		if (blur === 'blur') {
			right_side += '00';
		}

		// Limit decimal to only 2 digits
		right_side = right_side.substring(0, 2);

		// join number by .
		input_val =left_side + '.' + right_side + ;
	} else {
		// no decimal entered
		// add commas to number
		// remove all non-digits
		input_val = formatNumber(input_val);
		input_val = '$' + input_val;

		// final formatting
		if (blur === 'blur') {
			input_val += '.00';
		}
	}

	// send updated string to input
	input.val(input_val);

	// put caret back in the right position
	var updated_len = input_val.length;
	caret_pos = updated_len - original_len + caret_pos;
	input[0].setSelectionRange(caret_pos, caret_pos);
}

var tt = tinhTongLuong();

var tongLuong = formatCurrency(tt);
console.log('file: main.js ~ line 184 ~ tongLuong', tongLuong);
