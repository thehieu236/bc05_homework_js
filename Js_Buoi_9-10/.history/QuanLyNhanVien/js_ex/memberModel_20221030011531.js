function member(
	username,
	nameFull,
	email,
	passWord,
	date,
	salary,
	sumSalary,
	position,
	workTime
) {
	this.username = username;
	this.nameFull = nameFull;
	this.email = email;
	this.passWord = passWord;
	this.date = date;
	this.salary = salary;
	this.sumSalary = function () {
		if (chucVu == 'Sếp') {
			return luongSep;
		} else if (chucVu == 'Trưởng phòng') {
			return luongTruongPhong;
		} else {
			return luongCB;
		}
	};
	this.position = position;
	this.workTime = workTime;
}
