var layThongTinNhanVien = function () {
	var username = document.getElementById('tknv').value;
	var nameFull = document.getElementById('name').value;
	var email = document.getElementById('email').value;
	var passWord = document.getElementById('password').value;
	var date = document.getElementById('datepicker').value;
	var salary = document.getElementById('luongCB').value;
	var position = document.getElementById('chucvu').value;
	var workTime = document.getElementById('gioLam').value;

	var nhanVien = new member(
		username,
		nameFull,
		email,
		passWord,
		date,
		salary,
		position,
		workTime
	);

	return nhanVien;
};

function renderDsnv(nvArr) {
	var contentHtml = '';
	for (i = 0; i < nvArr.length; i++) {
		var currentNv = nvArr[i];

		// Tạo biến gán giá trị là tổng lương.
		var tongLuong = currentNv.sumSalary();
		var contentTr = `
		<tr>
		<td>${currentNv.username}</td>
		<td>${currentNv.nameFull}</td>
		<td>${currentNv.email}</td>
		<td>${currentNv.workTime}</td>
		<td>${currentNv.position}</td>
		<td>${formatToVnd(tongLuong)} VND</td>
		<td>${currentNv.position}</td>
		<td>
		<button onclick="suaThongTin('${
			currentNv.username
		}')" class="btn btn-warning mb-2" id="btnThem" data-toggle="modal" data-target="#myModal">Sửa</button>

		<buton onclick="xoaThongTin('${
			currentNv.username
		}')" class='btn btn-danger mb-2'>Xoá</buton>
		</td>
		
		</tr>
		`;

		contentHtml += contentTr;
	}

	document.getElementById('tableDanhSach').innerHTML = contentHtml;
}

function showThongTinLenForm(nv) {
	document.getElementById('tknv').value = nv.username;
	document.getElementById('name').value = nv.nameFull;
	document.getElementById('email').value = nv.email;
	document.getElementById('password').value = nv.passWord;
	document.getElementById('datepicker').value = nv.date;
	document.getElementById('luongCB').value = nv.salary;
	document.getElementById('chucvu').value = nv.position;
	document.getElementById('gioLam').value = nv.workTime;
}

function timViTri(username, nvArr) {
	for (let index = 0; index < nvArr.length; index++) {
		const item = nvArr[index];

		// Kiểm tra nếu tên tài khoản xuất hiện thì trả về vị trí index trong mảng của nó.
		if (item.username == username) {
			return index;
		}
	}

	// Nếu không tìm thấy thì trả về -1
	return -1;
}

function showMessageErr(idErr, display, messagerErr) {
	var spThangBao = document.getElementById(idErr);
	spThangBao.style.display = display;
	spThangBao.innerHTML = `${messagerErr}`;
}

function resetForm() {
	document.getElementById('modal__form').reset();
}
