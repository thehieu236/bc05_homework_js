/*
4. Validation

+ Tài khoản tối đa 4 - 6 ký số, không để trống
+ Tên nhân viên phải là chữ, không để trống
+ Email phải đúng định dạng, không để trống
+ mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt), không để trống
+ Ngày làm không để trống, định dạng mm/dd/yyyy
+ Lương cơ bản 1 000 000 - 20 000 000, không để trống
+ Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)
+ Số giờ làm trong tháng 80 - 200 giờ, không để trống
*/

function kiemTraTrung(username, dsnv) {
	var index = timViTri(username, dsnv);

	if (index !== -1) {
		showMessageErr(
			'tbTKNV',
			'block',
			'Tên tài khoản đã tồn tại, vui lòng nhập lại.'
		);
		return false;
	}
	showMessageErr('tbTKNV', 'none', '');

	return true;
}

function kiemTraRong(valueInput, idErr, messagerErr) {
	if (valueInput.length === 0) {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
	if (valueInput == 'Chọn chức vụ') {
		showMessageErr(idErr, 'block', messagerErr);
	} else {
		showMessageErr(idErr, 'none', '');
		return true;
	}
}

// + Tài khoản tối đa 4 - 6 ký số, không để trống
function kiemTraKyTuUsename(valueInput, idErr) {
	var reg = /^[a-z0-9_-]{4,6}$/;
	var isUsername = reg.test(valueInput);
	if (isUsername) {
		showMessageErr(idErr, '');
		return true;
	} else {
		showMessageErr(
			idErr,
			'block',
			'Vui lòng nhập từ 4 đến 6 ký tự, tên tài khoản không được viết hoa.'
		);
		return false;
	}
}

// + Tên nhân viên phải là chữ, không để trống
function kiemTraTenNhanVien(valueInput, idErr, messagerErr) {
	// Tách value thành mảng.
	const arrValueInput = valueInput.split('');

	// Tạo vòng lặp kiểm tra từng ký tự trong mảng mới tạo.
	for (let i = 0; i < arrValueInput.length; i++) {
		const item = arrValueInput[i];
		const itemConvertNumber = function () {
			// Trường hợp có dấu cách nó sẽ trả về 0 nên ta tạo điều kiện
			if (item === ' ') {
				//Nếu có dấu cách thì trả về NaN.
				return NaN;
			}

			return Number(item);
		}; //Chuyển đổi các ký tự thành số.

		if (isNaN(itemConvertNumber()) == false) {
			//Nếu không phải là số thì ta kết thúc vòng lặp, trả về kết quả false và hiển thị thông báo
			showMessageErr(idErr, 'block', messagerErr);
			return false;
		}
	}

	showMessageErr(idErr, 'none', ' ');
	return true;
}

// + Email phải đúng định dạng
function kiemTraEmail(valueInput, idErr, messagerErr) {
	var reg =
		/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var isEmail = reg.test(valueInput);
	if (isEmail) {
		showMessageErr(idErr, 'none', '');
		return true;
	} else {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
}

// + Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)
function kiemTraPassWord(valueInput, idErr, messagerErr) {
	var reg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,10}$/;
	var isPassWord = reg.test(valueInput);
	if (isPassWord) {
		showMessageErr(idErr, 'none', '');
		return true;
	} else {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
}

// + Ngày làm không để trống, định dạng mm/dd/yyyy
function kiemTraNgay(valueInput, idErr, messagerErr) {
	var reg = /^[0-9]{2}[\/]{1}[0-9]{2}[\/]{1}[0-9]{4}$/g;
	var isDate = reg.test(valueInput);
	console.log('file: validation.js ~ line 119 ~ kiemTraNgay ~ isDate', isDate);
	if (isDate) {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	} else {
		showMessageErr(idErr, 'none', messagerErr);
		return true;
	}
}

// + Lương cơ bản 1 000 000 - 20 000 000, không để trống
function kiemTraMucLuong(valueInput, idErr, messagerErr) {
	if (valueInput > 1000000 && valueInput < 20000000) {
		showMessageErr(idErr, 'none', messagerErr);
		return true;
	} else {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
}
	
// + Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)
function kiemTraChucVu(valueInput, idErr, messagerErr) {

	if (valueInput == 'Chọn chức vụ') {
		showMessageErr(idErr, 'block', messagerErr);
		return false
	} else {
		showMessageErr(idErr, 'none', '');
		return true;
	}
}
	
// + Số giờ làm trong tháng 80 - 200 giờ, không để trống
function kiemTraGioLam(valueInput, idErr, messagerErr) {
	if (valueInput > 80 && valueInput < 200) {
		showMessageErr(idErr, 'block', messagerErr)
		return false
	} else {
		showMessageErr
	}
}
