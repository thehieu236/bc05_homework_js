/*
4. Validation

+ Tài khoản tối đa 4 - 6 ký số, không để trống
+ Tên nhân viên phải là chữ, không để trống
+ Email phải đúng định dạng, không để trống
+ mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt), không để trống
+ Ngày làm không để trống, định dạng mm/dd/yyyy
+ Lương cơ bản 1 000 000 - 20 000 000, không để trống
+ Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)
+ Số giờ làm trong tháng 80 - 200 giờ, không để trống
*/

function kiemTraTrung(username, dsnv) {
	var index = timViTri(username, dsnv);

	if (index !== -1) {
		showMessageErr(
			'tbTKNV',
			'block',
			'Tên tài khoản đã tồn tại, vui lòng nhập lại.'
		);
		return false;
	}
	showMessageErr('tbTKNV', 'none', '');

	return true;
}

function kiemTraRong(valueInput, idErr, messagerErr) {
	if (valueInput.length === 0) {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
	if (valueInput == 'Chọn chức vụ') {
		showMessageErr(idErr, 'block', messagerErr);
	} else {
		showMessageErr(idErr, 'none', '');
		return true;
	}
}

// + Tài khoản tối đa 4 - 6 ký số, không để trống.
function kiemTraKyTuUsename(valueInput, idErr) {
	var reg = /^[a-z0-9_-]{4,6}$/;
	var isUsername = reg.test(valueInput);
	if (isUsername) {
		showMessageErr(idErr, '');
		return true;
	} else {
		showMessageErr(
			idErr,
			'block',
			'Vui lòng nhập từ 4 đến 6 ký tự, tên tài khoản không được viết hoa.'
		);
		return false;
	}
}

// + Tên nhân viên phải là chữ.
function kiemTraTenNhanVien(valueInput, idErr, messagerErr) {
	// Tách value thành mảng.
	const arrValueInput = valueInput.split('');

	// Tạo vòng lặp kiểm tra từng ký tự trong mảng mới tạo.
	for (let i = 0; i < arrValueInput.length; i++) {
		const item = arrValueInput[i];
		const itemConvertNumber = function () {
			// Trường hợp có dấu cách nó sẽ trả về 0 nên ta tạo điều kiện
			if (item === ' ') {
				//Nếu có dấu cách thì trả về NaN.
				return NaN;
			}

			return Number(item);
		}; //Chuyển đổi các ký tự thành số.

		if (isNaN(itemConvertNumber()) == false) {
			//Nếu không phải là số thì ta kết thúc vòng lặp, trả về kết quả false và hiển thị thông báo.
			showMessageErr(idErr, 'block', messagerErr);
			return false;
		}
	}

	showMessageErr(idErr, 'none', ' ');
	return true;
}

// + Email phải đúng định dạng.
function kiemTraEmail(valueInput, idErr, messagerErr) {
	var reg =
		/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var isEmail = reg.test(valueInput);
	if (isEmail) {
		showMessageErr(idErr, 'none', '');
		return true;
	} else {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
}

// + Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt).
function kiemTraPassWord(valueInput, idErr, messagerErr) {
	var reg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,10}$/;
	var isPassWord = reg.test(valueInput);
	if (isPassWord) {
		showMessageErr(idErr, 'none', '');
		return true;
	} else {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
}

// + Ngày làm không để trống, định dạng mm/dd/yyyy.
function kiemTraNgay(valueInput, idErr, messagerErr) {
	var reg =
		/(^(((0[1-9]|1[012])[-/.](0[1-9]|1[0-9]|2[0-8]))|((0[13578]|1[02])[-/.](29|30|31))|((0[4,6,9]|11)[-/.](29|30)))[-/.](19|[2-9][0-9])\d\d$)|(^02[-/.]29[-/.](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/;
	var isDate = reg.test(valueInput);
	if (isDate) {
		showMessageErr(idErr, 'none', messagerErr);
		return true;
	} else {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
}

// + Lương cơ bản 1 000 000 - 20 000 000.
function kiemTraMucLuong(valueInput, idErr, messagerErr) {
	// Vì value input đầu vào là string nên ta đổi sang number để kiểm tra.
	var arrValueInput = valueInput.split(',');
	var number = arrValueInput.join('');
	number = number * 1;

	if (number >= 1000000 && number <= 20000000) {
		showMessageErr(idErr, 'none', messagerErr);
		return true;
	} else {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
}

// + Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên).
function kiemTraChucVu(valueInput, idErr, messagerErr) {
	if (valueInput == 'Chọn chức vụ') {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	} else {
		showMessageErr(idErr, 'none', '');
		return true;
	}
}

// + Số giờ làm trong tháng 80 - 200 giờ,..
function kiemTraGioLam(valueInput, idErr, messagerErr) {
	if (valueInput >= 80 && valueInput <= 200) {
		showMessageErr(idErr, 'none', messagerErr);
		return true;
	} else {
		showMessageErr(idErr, 'block', messagerErr);
		return false;
	}
}
