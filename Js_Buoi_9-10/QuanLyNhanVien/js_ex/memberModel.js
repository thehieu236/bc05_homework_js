function member(
	username,
	nameFull,
	email,
	passWord,
	date,
	salary,
	position,
	workTime
) {
	this.username = username;
	this.nameFull = nameFull;
	this.email = email;
	this.passWord = passWord;
	this.date = date;
	this.salary = salary;
	this.position = position;
	this.workTime = workTime;

/*
5. Xây dựng phương thức tính tổng lương cho đối tượng nhân viên.

+ nếu chức vụ là giám đốc: tổng lương = lương cơ bản * 3.
+nếu chức vụ là trưởng phòng: tổng lương = lương cơ bản * 2.
+nếu chức vụ là nhân viên: tổng lương = lương cơ bản *.
*/
	this.sumSalary = function () {
		luongCB = formatToNumber(salary);
		// Tính tổng lương theo chức vụ.
		var luongSep = luongCB * 3;
		var luongTruongPhong = luongCB * 2;

		if (position == 'Sếp') {
			return luongSep;
		} else if (position == 'Trưởng phòng') {
			return luongTruongPhong;
		} else {
			return luongCB;
		}
	};

	/**
	 * 
*6. Xây dựng phương thức xếp loại cho đối tượng nhân viên:

* + nếu nhân viên có giờ làm trên 192h(>= 192): nhân viên xuất sắc.
* + nếu nhân viên có giờ làm trên 176h (>=176): nhân viên giỏi.
* + nếu nhân viên có giờ làm trên 160h (>=160): nhân viên khá.
* + nếu nhân viên có giờ làm dưới 160h: nhân viên trung bình.
	 */
	this.rank = function () {
		if (workTime >= 192) {
			return 'Xuất sắc';
		} else if (workTime >= 176) {
			return 'Giỏi';
		} else if (workTime >= 160) {
			return 'Khá';
		} else {
			return 'Trung bình';
		}
	};
}
