/**
 * Bài 1: Tính tiền lương nhân viên.
 * Input: Nhập số ngày làm (28 ngày)
 *
 * Step:
 *  +Step 1: Tạo biến workDay = 28
 *  +Step 2: Tạo biến salary = 100.000
 *  +Step 3: Nhân workDay với salary
 *
 *
 * Output: Tổng số tiền lương nhận được.(2.800.000)
 */

var workDay = 28;
var salary = 100000;

var result = null;
result = workDay * salary;

console.log('result: ', result);

/**
 * Bài 2: Tính giá trị trung bình.
 *
 * Input: 1 2 3 4 5
 *
 * Step:
 *  + Step 1: Tạo các biến.
 *  + Step 2: Cộng các biến lại rồi chia cho 5.
 *
 * Output: 3
 */

var a = 1;
var b = 2;
var c = 3;
var d = 4;
var e = 5;
var result_2 = null;

result_2 = (a + b + c + d + e) / 5;

console.log('result_2: ', result_2);

/**
 * Bài 3: Quy đổi tiền.
 *
 * Input: Người dùng nhập vào số tiền USD: 2
 *
 * Step:
 *  + Step 1: Tạo biến USD
 *  + Step 2: Tạo biến VND
 *  + Step 3: Nhân USD với VND
 *
 * Output: 47.000
 */

var USD = 2;
var VND = 23500;

var result_3 = null;

result_3 = USD * VND;
console.log('result_3: ', result_3);

/**
 * Bài 4: Tính diện tích, chu vi hình chữ nhật.
 * Input: Width = 4; Height = 5
 *
 * Step:
 *  + Step 1: Tạo biến width
 *  + Step 2: Tạo biến height
 *  + Step 3: Tạo biến resultArea và resultPerimeter
 *  + Step 4: resultArea = width * height
 *  + Step 3: resultPerimeter = (width + height) / 2
 *
 * Output: resultArea = 20
 *         resultPerimeter = 4.5
 */

var width = 4;
var height = 5;

var resultArea = null;
var resultPerimeter = null;

resultArea = width * height;
resultPerimeter = (width + height) / 2;

console.log('resultArea: ', resultArea);
console.log('resultPerimeter: ', resultPerimeter);

/**
 * Bài 5: Tính tổng hai ký số.
 * Input: 83
 *
 * Step:
 *  + Step 1: Tạo biến number = 12.
 *  + Step 2: Lấy số hàng đơn vị.
 *  + Step 3: Lấy số hàng đơn chục.
 *  + Step 4: Cộng hai số lại.
 *
 * Output: 11
 */

var number = 83;
var unit = number % 10;
var ten = Math.floor(number / 10);

var result_5 = null;

result_5 = unit + ten;
console.log('result_5: ', result_5);
